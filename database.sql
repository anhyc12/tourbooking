﻿ create database TourBooking;

use TourBooking

create table Statuses(
ID int primary key identity,
StatusType nvarchar(32)
)

create table Accounts(
ID int primary key identity,
StatusID int FOREIGN KEY REFERENCES Statuses(ID),
Username nvarchar(32),
Password nvarchar(32),
Role bit
)

create table Users(
UserId int primary key,
Fullname nvarchar(32),
Gender bit,
PhoneNumber nvarchar(12),
Email nvarchar(32) not null,
Address nvarchar(32),
FOREIGN KEY (UserId) REFERENCES Accounts(ID)
)


create table Tours(
ID int primary key identity,
StatusID int FOREIGN KEY REFERENCES Statuses(ID),
Name nvarchar(32),
Price float,
Image nvarchar(32),
Description nvarchar(32),
Category nvarchar(32),
Location nvarchar(32),
[StartDate] [date],
[EndDate] [date],
)

create table Bookings(
ID int primary key identity,
TourID int FOREIGN KEY REFERENCES Tours(ID),
AccountID int FOREIGN KEY REFERENCES Accounts(ID),
StatusID int FOREIGN KEY REFERENCES Statuses(ID),
StartDate datetime,
DueDate datetime,
PersonNumber int,
Price money
)

create table Blogs(
ID int primary key identity,
StatusID int FOREIGN KEY REFERENCES Statuses(ID),
Title nvarchar(400),
Image nvarchar(4000),
Date datetime,
Content nvarchar(4000),
Category nvarchar(400),
)

create table Reviews(
ID int primary key identity,
AccountID int FOREIGN KEY REFERENCES Accounts(ID),
TourID int FOREIGN KEY REFERENCES Tours(ID),
StatusID int FOREIGN KEY REFERENCES Statuses(ID),
Rate float,
Comment nvarchar(32),
ReviewDate date
)
ALTER TABLE Accounts
ADD Code nvarchar(6),
    ExpiredTime datetime;

insert into Statuses values ('ok');
insert into Statuses values ('notok');
insert into Accounts values (1, 'mkboss', '123', 1 , null, null);
insert into Users values( 49, 'Minh Thanh' , '1' , '0123456789' , 'minhthanh@123.gmail.com' , 'Ha Noi')
INSERT [dbo].[Tours] ([StatusID], [Name], [Price], [Image], [Description], [Category], [Location], [StartDate], [EndDate]) VALUES (1, N'hanoi', 100, N'sdadad', N'asdad', N'asdadad', N'asdad', CAST(N'2002-02-11' AS Date), CAST(N'2002-01-13' AS Date))
INSERT [dbo].[Tours] ([StatusID], [Name], [Price], [Image], [Description], [Category], [Location], [StartDate], [EndDate]) VALUES (1, N'hcm', 124, N'124ffsfd', N'sdfgergbfd', N'agfsdg', N'dsfg', CAST(N'2010-02-11' AS Date), CAST(N'2010-05-13' AS Date))
insert into Bookings values (1, 1, 1, '2002-9-28', '2002-11-2', 2, 100);
insert into Blogs values (1,'Hello','Hello.png','2002-9-28','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non dui sodales, faucibus libero ut, posuere felis','News');
insert into Blogs values (1,'Hello','Hello.png','2002-9-28','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non dui sodales, faucibus libero ut, posuere felis','News');
insert into Blogs values (1,'Hello','Hello.png','2002-9-28','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non dui sodales, faucibus libero ut, posuere felis','News');
insert into Blogs values (1,'Hello','Hello.png','2002-9-28','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non dui sodales, faucibus libero ut, posuere felis','News');
insert into Blogs values (1,'Hello','Hello.png','2002-9-28','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non dui sodales, faucibus libero ut, posuere felis','News');

alter table Bookings
add LastName nvarchar(32),  FirstName nvarchar(32), Email nvarchar(32), PhoneNumber nvarchar(12);




insert into Statuses values ('actived'), ('non-actived')


