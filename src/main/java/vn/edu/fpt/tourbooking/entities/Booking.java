/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.edu.fpt.tourbooking.entities;

import java.sql.Date;

/**
 *
 * @author Admin
 */
public class Booking {
    //properties
    private int bookingID;
    private Date startDate;
    private Date dueDate;
    private int numberPerson;
    private double price;
    private Account account;
    private Tour tour;
    private Status status;
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;

    //no args constructer
    public Booking() {
    }
    
    //all args constructor
    public Booking(int bookingID, Date startDate, Date dueDate, int numberPerson, double price, Account account, Tour tour, Status status, String firstName, String lastName, String email, String phoneNumber) {
        this.bookingID = bookingID;
        this.startDate = startDate;
        this.dueDate = dueDate;
        this.numberPerson = numberPerson;
        this.price = price;
        this.account = account;
        this.tour = tour;
        this.status = status;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }
    
    //bookingID getter
    public int getBookingID() {
        return bookingID;
    }

    //bookingID setter
    public void setBookingID(int bookingID) {
        this.bookingID = bookingID;
    }

    //startDate getter
    public Date getStartDate() {
        return startDate;
    }

    //startDate setter
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    //dueDate getter
    public Date getDueDate() {
        return dueDate;
    }

    //dueDate setter
    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    //numberPerson getter
    public int getNumberPerson() {
        return numberPerson;
    }
    
    //numberPerson setter
    public void setNumberPerson(int numberPerson) {
        this.numberPerson = numberPerson;
    }

    //price getter
    public double getPrice() {
        return price;
    }

    //price setter
    public void setPrice(double price) {
        this.price = price;
    }

    //account getter
    public Account getAccount() {
        return account;
    }

    //account setter
    public void setAccount(Account account) {
        this.account = account;
    }

    //tour getter
    public Tour getTour() {
        return tour;
    }

    //tour setter
    public void setTour(Tour tour) {
        this.tour = tour;
    }

    //status getter
    public Status getStatus() {
        return status;
    }

    //status setter
    public void setStatus(Status status) {
        this.status = status;
    }

    //firstName getter
    public String getFirstName() {
        return firstName;
    }

    //firstName setter
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    //lastName getter
    public String getLastName() {
        return lastName;
    }

    //lastName setter
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    //email getter
    public String getEmail() {
        return email;
    }

    //email setter
    public void setEmail(String email) {
        this.email = email;
    }

    //phoneNumber getter
    public String getPhoneNumber() {
        return phoneNumber;
    }

    //phoneNumber setter
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    
    
}
