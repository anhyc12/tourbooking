/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.edu.fpt.tourbooking.entities;

/**
 *
 * @author Admin
 */
public class Status {
    private int Id;
    private String statusType;

    public Status() {
    }
 
    public Status(int Id, String statusType) {
        this.Id = Id;
        this.statusType = statusType;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getStatusType() {
        return statusType;
    }

    public void setStatusType(String statusType) {
        this.statusType = statusType;
    }
    
    
}
