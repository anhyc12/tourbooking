/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.edu.fpt.tourbooking.entities;

import java.sql.Date;
import java.sql.Timestamp;


/**
 *
 * @author Admin
 */
public class Account {
    private int Id;
    private String username;
    private String password;
    private boolean role;
    private Status status;
    private String code;
    private Timestamp expiredTime;
    public Account(int id, String username, String password, boolean role, Status status, String code, Timestamp expiredTime) {
        this.Id = id;
        this.username = username;
        this.password = password;
        this.role = role;
        this.status = status;
        this.code = code;
        this.expiredTime = expiredTime;
    }

    public Account() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isRole() {
        return role;
    }

    public void setRole(boolean role) {
        this.role = role;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Timestamp getExpiredTime() {
        return expiredTime;
    }

    public void setExpiredTime(Timestamp expiredTime) {
        this.expiredTime = expiredTime;
    }
    @Override
    public String toString() {
        return "Account{" + "Id=" + Id + ", username=" + username + ", password=" + password + ", role=" + role + ", status=" + status + '}';
    }
    
    
}
