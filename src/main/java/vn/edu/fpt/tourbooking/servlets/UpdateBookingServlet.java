/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package vn.edu.fpt.tourbooking.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import vn.edu.fpt.tourbooking.dto.AccountDTO;
import vn.edu.fpt.tourbooking.dto.BookingDTO;
import vn.edu.fpt.tourbooking.dto.TourDTO;
import vn.edu.fpt.tourbooking.services.AccountService;
import vn.edu.fpt.tourbooking.services.BookingService;
import vn.edu.fpt.tourbooking.services.TourService;
import vn.edu.fpt.tourbooking.services.impl.AccountServiceImpl;
import vn.edu.fpt.tourbooking.services.impl.BookingServiceImpl;
import vn.edu.fpt.tourbooking.services.impl.TourServiceImpl;

/**
 *
 * @author Admin
 */
public class UpdateBookingServlet extends HttpServlet {

    private BookingService bookingService = new BookingServiceImpl();
    private TourService tourService = new TourServiceImpl();
    private AccountService accountService = new AccountServiceImpl();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UpdateBookingServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UpdateBookingServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    //check authen when get request 
    private AccountDTO authentication(HttpSession session) throws IOException {
        AccountDTO account = (AccountDTO) session.getAttribute("account");
        if (account != null) {
            AccountDTO checkAccount = accountService.getAccount(account.getUsername(), account.getPassword());
            if (checkAccount != null) {
                return checkAccount;
            }
        }
        return null;
    }

    private boolean authorization(boolean role){
        return role == false;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //get booking dto by id 
        int bookingId = Integer.parseInt(request.getParameter("id"));
        BookingDTO booking = bookingService.getBookingByID(bookingId);
        //get tour dto by id
        TourDTO tour = tourService.getTourByID(booking.getTourID());
        //get account from session
        HttpSession session = request.getSession();
        AccountDTO account = (AccountDTO) session.getAttribute("account");
        if (account != null) {
            if (authorization(account.isRole())) {
                //set attribute to request
                request.setAttribute("bookingDTO", booking);
                request.setAttribute("tourDTO", tour);
                //sende request dispatcher to update.jsp
                request.getRequestDispatcher("booking/update.jsp").forward(request, response);
            }
            response.sendRedirect("/TourBooking/UnauthorizedServlet");
        }else{
            response.sendRedirect("/TourBooking/login");
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //get parameter from reqest
        int numberPerson = Integer.parseInt(request.getParameter("inpNumberPerson"));
        String firstName = request.getParameter("inpFirstName");
        String lastName = request.getParameter("inpLastName");
        String email = request.getParameter("inpEmail");
        String phoneNumber = request.getParameter("inpPhoneNumber");
        double price = Double.parseDouble(request.getParameter("inpPrice"));
        int bookingId = Integer.parseInt(request.getParameter("id"));
        //get booking dto by id
        BookingDTO booking = bookingService.getBookingByID(bookingId);
        //set new data to booking dto
        booking.setNumberPerson(numberPerson);
        booking.setFirstName(firstName);
        booking.setLastName(lastName);
        booking.setEmail(email);
        booking.setPhoneNumber(phoneNumber);
        booking.setPrice(price);
        //update booking 
        BookingDTO update = bookingService.updateBooking(booking);
        //send redirect to ViewBookingServlet
        response.sendRedirect("/TourBooking/ViewBookingServlet");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
