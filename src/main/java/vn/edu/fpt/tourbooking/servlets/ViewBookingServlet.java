/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package vn.edu.fpt.tourbooking.servlets;

import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import vn.edu.fpt.tourbooking.dto.AccountDTO;
import vn.edu.fpt.tourbooking.dto.BookingDTO;
import vn.edu.fpt.tourbooking.services.AccountService;
import vn.edu.fpt.tourbooking.services.BookingService;
import vn.edu.fpt.tourbooking.services.impl.AccountServiceImpl;
import vn.edu.fpt.tourbooking.services.impl.BookingServiceImpl;

/**
 *
 * @author Admin
 */
public class ViewBookingServlet extends HttpServlet {

    private BookingService bookingService = new BookingServiceImpl();
    private AccountService accountService = new AccountServiceImpl();

    //check authen when get request 
    private AccountDTO authentication(HttpSession session) throws IOException {
        AccountDTO account = (AccountDTO) session.getAttribute("account");
        if (account != null) {
            AccountDTO checkAccount = accountService.getAccount(account.getUsername(), account.getPassword());
            if (checkAccount != null) {
                return checkAccount;
            }
        }
        return null;
    }

    private boolean authorization(boolean role){
        return role == false;
    }

    //get request
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
//        AccountDTO account = (AccountDTO) session.getAttribute("account");
        AccountDTO account = authentication(session);
        if (account != null) {
            if (authorization(account.isRole())) {
                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    String from = req.getParameter("from");
                    String to = req.getParameter("to");
                    List<BookingDTO> list = null;
                    
                    if (from != null && to != null && !from.isEmpty() && !to.isEmpty())
                        list = bookingService.getAllBookingByDate(new Date(sdf.parse(from).getTime()), new Date(sdf.parse(to).getTime()));
                    else
                        list = bookingService.getAllBookingByAccountID(account.getId());                    
                    
                    req.setAttribute("listBooking", list);
                    req.getRequestDispatcher("booking/index.jsp").forward(req, resp);
                } catch (ParseException ex) {
                    Logger.getLogger(ViewBookingServlet.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            resp.sendRedirect("/TourBooking/UnauthorizedServlet");
        }else{
            resp.sendRedirect("/TourBooking/login");
        }
    }
}
