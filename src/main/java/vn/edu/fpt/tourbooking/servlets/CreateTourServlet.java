/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package vn.edu.fpt.tourbooking.servlets;

import javax.servlet.annotation.MultipartConfig;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import vn.edu.fpt.tourbooking.dao.TourDAO;
import vn.edu.fpt.tourbooking.dao.impl.TourDAOImpl;
import vn.edu.fpt.tourbooking.dto.AccountDTO;
import vn.edu.fpt.tourbooking.dto.TourDTO;
import vn.edu.fpt.tourbooking.entities.Status;
import vn.edu.fpt.tourbooking.entities.Tour;
import vn.edu.fpt.tourbooking.services.TourService;
import vn.edu.fpt.tourbooking.services.impl.TourServiceImpl;

/**
 *
 * @author Admin
 */
@WebServlet(name = "CreateTourServlet", urlPatterns = {"/CreateTourServlet"})
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, //2MB
        maxFileSize = 1024 * 1024 * 50, // 50MB
        maxRequestSize = 1024 * 1024 * 50) // 50MB
public class CreateTourServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ViewBookingServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ViewBookingServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        processRequest(request, response);
        HttpSession session = request.getSession();
        AccountDTO account = (AccountDTO) session.getAttribute("account");

        if (account != null && account.isRole()) {
            request.getRequestDispatcher("admin/createtour.jsp").forward(request, response);
        } else {
            response.sendRedirect("login");
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String tStatus = request.getParameter("tStatus");
        String name = request.getParameter("tName");
        String priceStr = request.getParameter("tPrice");
        double price = Double.parseDouble(priceStr);
        String image = request.getParameter("myFile");
        String category = request.getParameter("category");
        String description = request.getParameter("description");
        String location = request.getParameter("location");
        String StartDate = request.getParameter("startDate");
        String EndDate = request.getParameter("endDate");

        Part filePart = request.getPart("myFile1");
        String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
        filePart.write(getServletContext().getRealPath(File.separator) + "\\..\\..\\src\\main\\webapp\\images\\" + fileName);

        TourDTO tour = new TourDTO("ok", name, price, image, category, description, location, StartDate, EndDate);
        TourService t = new TourServiceImpl();
        t.createTour(tour);

        response.sendRedirect("ViewTourServlet");

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
