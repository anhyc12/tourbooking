/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package vn.edu.fpt.tourbooking.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.util.Calendar;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import vn.edu.fpt.tourbooking.dto.AccountDTO;
import vn.edu.fpt.tourbooking.dto.BookingDTO;
import vn.edu.fpt.tourbooking.dto.TourDTO;
import vn.edu.fpt.tourbooking.services.AccountService;
import vn.edu.fpt.tourbooking.services.BookingService;
import vn.edu.fpt.tourbooking.services.TourService;
import vn.edu.fpt.tourbooking.services.impl.AccountServiceImpl;
import vn.edu.fpt.tourbooking.services.impl.BookingServiceImpl;
import vn.edu.fpt.tourbooking.services.impl.TourServiceImpl;

/**
 *
 * @author Admin
 */
public class CreateBookingServlet extends HttpServlet {

    private BookingService bookingService = new BookingServiceImpl();
    private TourService tourService = new TourServiceImpl();
    private AccountService accountService = new AccountServiceImpl();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CreateBookingServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CreateBookingServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    //check authen when get request 
    private AccountDTO authentication(HttpSession session) throws IOException {
        AccountDTO account = (AccountDTO) session.getAttribute("account");
        if (account != null) {
            AccountDTO checkAccount = accountService.getAccount(account.getUsername(), account.getPassword());
            if (checkAccount != null) {
                return checkAccount;
            }
        }
        return null;
    }

    private boolean authorization(boolean role){
        return role == false;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //Get tour by tour id
        int tourId = Integer.parseInt(request.getParameter("tourId"));
        TourDTO tour = tourService.getTourByID(tourId);
        //Get account from session
        HttpSession session = request.getSession();
        AccountDTO account = (AccountDTO) session.getAttribute("account");
        if (account != null) {
            if (authorization(account.isRole())) {
                //set attribute to request
                request.setAttribute("tourDTO", tour);
                //get request dispatcher to create.jsp
                request.getRequestDispatcher("booking/create.jsp").forward(request, response);
            }
            response.sendRedirect("/TourBooking/UnauthorizedServlet");
        }else{
            response.sendRedirect("/TourBooking/login");
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        BookingDTO bookingDTO = new BookingDTO();
        //Get account from session
        HttpSession session = request.getSession();
        AccountDTO account = (AccountDTO) session.getAttribute("account");
        //Get start date and due date
        Calendar cal = Calendar.getInstance();
        Date startDate = new Date(cal.getTime().getTime());
        cal.add(Calendar.DATE, 7);
        Date dueDate = new Date(cal.getTime().getTime());
        //Get parameter from request 
        int tourId = Integer.parseInt(request.getParameter("tourId"));
        int numberPerson = Integer.parseInt(request.getParameter("inpNumberPerson"));
        String firstName = request.getParameter("inpFirstName");
        String lastName = request.getParameter("inpLastName");
        String email = request.getParameter("inpEmail");
        String phoneNumber = request.getParameter("inpPhoneNumber");
        double price = Double.parseDouble(request.getParameter("inpPrice"));
        //Set data to booking dto
        bookingDTO.setBookingID(0);
        bookingDTO.setAccountID(account.getId());
        bookingDTO.setTourID(tourId);
        bookingDTO.setStartDate(startDate);
        bookingDTO.setDueDate(dueDate);
        bookingDTO.setPrice(price);
        bookingDTO.setNumberPerson(numberPerson);
        bookingDTO.setFirstName(firstName);
        bookingDTO.setLastName(lastName);
        bookingDTO.setEmail(email);
        bookingDTO.setPhoneNumber(phoneNumber);
        bookingDTO.setStatusType("ok");
        //insert booking
        bookingService.insertBooking(bookingDTO);
        //go to ViewBookingServlet page
        response.sendRedirect("/TourBooking/ViewBookingServlet");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
