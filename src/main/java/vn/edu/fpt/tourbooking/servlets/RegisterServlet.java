/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package vn.edu.fpt.tourbooking.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Random;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import vn.edu.fpt.tourbooking.dto.AccountDTO;
import vn.edu.fpt.tourbooking.dto.UserDTO;
import vn.edu.fpt.tourbooking.entities.User;
import vn.edu.fpt.tourbooking.services.AccountService;
import vn.edu.fpt.tourbooking.services.UserService;
import vn.edu.fpt.tourbooking.services.impl.AccountServiceImpl;
import vn.edu.fpt.tourbooking.services.impl.UserServiceImpl;

/**
 *
 * @author Admin
 */
public class RegisterServlet extends HttpServlet {
    private AccountService accountService = new AccountServiceImpl();
    private UserService userService = new UserServiceImpl();
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ViewBookingServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ViewBookingServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("home/register.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = request.getParameter("usernameInput");
        String pwd = request.getParameter("passwordInput");
        String email = request.getParameter("emailInput");
        
        // validate unique information
        AccountDTO account = accountService.getAccountByUsername(username);
        if(account!=null){           
            request.setAttribute("usernameError", "Username already exists");
            request.getRequestDispatcher("home/register.jsp").forward(request, response);
        }
        UserDTO user = userService.getUserByEmail(email);
        if(user!=null){
            request.setAttribute("emailError", "Email is already exists");
            request.getRequestDispatcher("home/register.jsp").forward(request, response);
        }
        // Lấy thời gian hiện tại
        Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

        Calendar c = Calendar.getInstance();
        c.setTime(currentTimestamp);
        c.add(Calendar.MINUTE, 2);
        Timestamp expirationTime = new Timestamp(c.getTimeInMillis());

        account = new AccountDTO(username, pwd, getRandomCode(), expirationTime);
        //System.out.print(account.getCode());
        accountService.createAccount(account);
        account = accountService.getAccount(username, pwd);
        user = new UserDTO(account.getId(), null, null, true, email, null);
        userService.createUser(user);                
        
        sendConfirmMail(account);
        response.sendRedirect("home");
        
    }
    private void sendConfirmMail(AccountDTO account){
        String link = "http://localhost:8080/TourBooking/confirm-account?accountid="+account.getId()+"&code=" + 
                account.getCode();
        
	String noiDung = "<p>Hello <strong>"+ account.getUsername()+"</strong>,</p>\r\n"
				+ "<p>Please click this link to activate your account <strong>"+
                "<a href=\""+link+"\">"+link+"</a>" +"</strong>";
        UserDTO user = userService.getUserById(account.getId());     
        accountService.sendConfirmMail(user.getEmail(), link, noiDung);
    }
    
    private String getRandomCode(){
                Random rd = new Random();
		String s1 = rd.nextInt(10) + "";
		String s2 = rd.nextInt(10) + "";
		String s3 = rd.nextInt(10) + "";
		String s4 = rd.nextInt(10) + "";
		String s5 = rd.nextInt(10) + "";
		String s6 = rd.nextInt(10) + "";
		
		String s =  s1+s2+s3+s4+s5+s6;
		return s;
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
