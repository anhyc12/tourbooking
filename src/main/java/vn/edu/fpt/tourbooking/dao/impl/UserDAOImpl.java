/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.edu.fpt.tourbooking.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import vn.edu.fpt.tourbooking.context.DBContext;
import vn.edu.fpt.tourbooking.dao.UserDAO;
import vn.edu.fpt.tourbooking.entities.User;

/**
 *
 * @author Hoang Quoc Viet
 */
public class UserDAOImpl implements UserDAO{
    Connection con = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    @Override
    public User updateUser(User user) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public User getUserById(int userId) {
        String query = "select * from users where UserId = ?";
        User a = null;
        try {            
            con = new DBContext().connection;

            ps = con.prepareStatement(query);
            ps.setInt(1, userId);     
            rs = ps.executeQuery();
            if (rs.next()) {

               a = new User(rs.getInt("UserId"), rs.getString("Fullname"), rs.getString("PhoneNumber"), rs.getBoolean("Gender"), rs.getString("Email"), rs.getString("Address"));
            }
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
         // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
         return a;
    }

    @Override
    public User getUserByEmail(String email) {
        String query = "select * from users where Email = ?";
        User u = null;
        try {            
            con = new DBContext().connection;

            ps = con.prepareStatement(query);
            ps.setString(1, email);
            
            rs = ps.executeQuery();
            if (rs.next()) {
               u = new User(rs.getInt("userid"), rs.getString("fullname"), rs.getString("phonenumber"), 
                       rs.getBoolean("gender"), rs.getString("email"), rs.getString("address"));
            }
            
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
         // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
         return u;
    }

    @Override
    public User insertUser(User user) {
        String query = "INSERT INTO [dbo].[Users] ([UserId],[Fullname],[Gender],[PhoneNumber],[Email],[Address])\n"
                + "     VALUES\n"
                + "           (?"
                + "           ,?"
                + "           ,?"
                + "           ,?"
                + "           ,?"
                + "	    ,?)";
        User u = null;
        try {            
            con = new DBContext().connection;

            ps = con.prepareStatement(query);
            ps.setInt(1, user.getUserId());
            ps.setString(2, user.getFullName());
            ps.setBoolean(3, user.isGender());
            ps.setString(4, user.getPhoneNumber());
            ps.setString(5, user.getEmail());
            ps.setString(6, user.getAddress());
            int affectedRows = ps.executeUpdate();
            if (affectedRows == 1) {
               u = user;
               System.out.print("Insert user successfully");
            }
            else{
                throw new SQLException("Insert failed, no rows affected.");
            }
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
         // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
         return u;
    }

    @Override
    public User deleteUser(User user) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<User> getAllUsers() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    
}
