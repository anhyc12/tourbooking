/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.edu.fpt.tourbooking.dao;

import vn.edu.fpt.tourbooking.entities.Blog;
import java.util.List;

/**
 *
 * @author M.S.I
 */
public interface BlogDAO {
    public void changeBlogStatus(int blogId);
    public int addBlog(Blog blog);
    public void updateBlogById(int blogId, Blog blog);
    public List<Blog> getAllBlog();
    public List<Blog> getBlogsByName(String name); 
}
