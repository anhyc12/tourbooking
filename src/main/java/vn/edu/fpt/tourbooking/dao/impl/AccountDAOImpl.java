/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.edu.fpt.tourbooking.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import vn.edu.fpt.tourbooking.context.DBContext;
import vn.edu.fpt.tourbooking.dao.AccountDAO;
import vn.edu.fpt.tourbooking.dao.StatusDAO;
import vn.edu.fpt.tourbooking.entities.Account;
import vn.edu.fpt.tourbooking.entities.Status;

/**
 *
 * @author Hoang Quoc Viet
 */
public class AccountDAOImpl implements AccountDAO{
    Connection con = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    StatusDAO s = new StatusDAOImpl();
    @Override
    public Account updateAccount(Account account) {
        String query = "UPDATE [dbo].[Accounts]\n"
                + "   SET [StatusID] = ?\n"
                + "      ,[Username] = ?\n"
                + "      ,[Password] = ?\n"
                + "      ,[Role] = ?\n"
                + "   WHERE id = ?";

        try {
            con = new DBContext().connection;
            ps = con.prepareStatement(query);
            ps.setInt(1, account.getStatus().getId());
            ps.setString(2, account.getUsername());
            ps.setString(3, account.getPassword());
            ps.setBoolean(4, account.isRole()) ;
            ps.setInt(5, account.getId());
            int affectedRows = ps.executeUpdate();
            if (affectedRows < 1) 
                throw new SQLException("Update account failed, no rows affected.");
            
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return account;
    }

    @Override
    public Account getAccount(String username, String password) {
        String query = "select * from accounts where username = ? and password = ?";
        Account a = null;
        try {            
            con = new DBContext().connection;

            ps = con.prepareStatement(query);
            ps.setString(1, username);
            ps.setString(2, password);       
            rs = ps.executeQuery();
            if (rs.next()) {
               a = new Account(rs.getInt("Id"), username, password, rs.getBoolean("role"), s.getStatusById(rs.getInt("statusId"))
               , rs.getString("code"), rs.getTimestamp("expiredtime"));
            }
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
         // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
         return a;
    }

    @Override
    public Account insertAccount(Account account) {
        String query = "INSERT INTO [dbo].[Accounts] "
                + "           ([StatusID] "
                + "           ,[Username] "
                + "           ,[Password] "
                + "           ,[Role] , [Code] , [ExpiredTime]) "
                + "     VALUES\n"
                + "           (?"
                + "           ,?"
                + "           ,?"
                + "           ,?"
                + "           ,?"
                + "           ,?)";

        try {
            con = new DBContext().connection;

            ps = con.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
            Status status = s.getStatusByType("non-actived");
            ps.setInt(1, status.getId());
            ps.setString(2, account.getUsername());
            ps.setString(3, account.getPassword());
            ps.setBoolean(4, account.isRole()) ;
            ps.setString(5, account.getCode()) ;
            ps.setTimestamp(6, account.getExpiredTime()) ;
            int affectedRows = ps.executeUpdate();
            if (affectedRows == 1) {
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    account.setId(rs.getInt(1));
                } else {
                    throw new SQLException("Insert failed, no ID obtained.");
                }
            } else {
                throw new SQLException("Insert failed, no rows affected.");
            }
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
            
        }
        return account;
         // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Account deleteAccount(Account account) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<Account> getAllAccounts() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Account getAccountById(int id) {
        String query = "select * from accounts where ID = ?";
        Account a = null;
        try {            
            con = new DBContext().connection;

            ps = con.prepareStatement(query);
            ps.setInt(1, id);     
            rs = ps.executeQuery();
            if (rs.next()) {
               StatusDAO s = new StatusDAOImpl();
               
               a = new Account(rs.getInt("Id"), rs.getString("username"), rs.getString("password"), rs.getBoolean("role"), s.getStatusById(rs.getInt("statusId"))
               , rs.getString("code"), rs.getTimestamp("expiredtime"));
            }
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
         // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
         return a;
    }

    @Override
    public int getStatisticAccount(int statusId) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    public static void main(String[] args) {
        AccountDAOImpl a = new AccountDAOImpl();
        StatusDAOImpl s = new StatusDAOImpl();
        Status status = s.getStatusByType("actived");
        Account account = a.getAccountById(19);
        account.setStatus(status);
        a.updateAccount(account);
        
    }

    @Override
    public Account getAccountByUsername(String username) {
        String query = "select * from accounts where username = ?";
        Account a = null;
        try {            
            con = new DBContext().connection;

            ps = con.prepareStatement(query);
            ps.setString(1, username);     
            rs = ps.executeQuery();
            if (rs.next()) {              
               a = new Account(rs.getInt("Id"), rs.getString("username"), rs.getString("password"), rs.getBoolean("role"), s.getStatusById(rs.getInt("statusId"))
               , rs.getString("code"), rs.getTimestamp("expiredtime"));
            }
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
         // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
         return a;
    }
}
