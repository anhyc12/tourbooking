/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.edu.fpt.tourbooking.dao;

import vn.edu.fpt.tourbooking.entities.Status;


/**
 *
 * @author Admin
 */
public interface StatusDAO {
    public Status getStatusByType(String statusType);
    public Status getStatusById(int statusId);
}
