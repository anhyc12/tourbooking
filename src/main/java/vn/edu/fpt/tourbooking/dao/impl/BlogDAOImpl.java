/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.edu.fpt.tourbooking.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import vn.edu.fpt.tourbooking.context.DBContext;
import vn.edu.fpt.tourbooking.dao.BlogDAO;
import vn.edu.fpt.tourbooking.dao.StatusDAO;
import vn.edu.fpt.tourbooking.entities.Blog;
import vn.edu.fpt.tourbooking.entities.Status;

/**
 *
 * @author M.S.I
 */
public class BlogDAOImpl implements BlogDAO {

    Connection con = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    @Override
    public void changeBlogStatus(int blogId) {
        String query = "UPDATE Blogs\n"
                + "SET StatusID = CASE\n"
                + "    WHEN StatusID = 1 THEN 2\n"
                + "    WHEN StatusID = 2 THEN 1\n"
                + "    ELSE StatusID \n"
                + "END\n"
                + "WHERE ID=?";
        try {
            con = new DBContext().connection;
            ps = con.prepareStatement(query);
            ps.setInt(1, blogId);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int addBlog(Blog blog) {
        String query = "insert into Blogs values "
                + "(?,"
                + "?,"
                + "?,"
                + "?,"
                + "?,"
                + "?)";
        try {
            con = new DBContext().connection;
            ps = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, 1);
            ps.setString(2, blog.getTitle());
            ps.setString(3, blog.getImage());
            ps.setObject(4, blog.getDate());
            ps.setString(5, blog.getContent());
            ps.setString(6, blog.getCategory());
            int affectedRows = ps.executeUpdate();
            if (affectedRows == 1) {
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    int generatedId = rs.getInt(1); // Lấy ID ở đây
                    return generatedId;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    Logger.getLogger(BlogDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return 0;
    }

    @Override
    public void updateBlogById(int blogId, Blog blog) {
        String query = "UPDATE Blogs\n"
                + "   SET "
                + "      [Title] = ?\n"
                + "      ,[Image] = ?\n"
                + "      ,[Date] = ?\n"
                + "      ,[Content] = ?\n"
                + "      ,[Category] = ?\n"
                + " WHERE [ID]=?";
        try {
            con = new DBContext().connection;
            ps = con.prepareStatement(query);
            ps.setString(1, blog.getTitle());
            ps.setString(2, blog.getImage());
            ps.setObject(3, blog.getDate());
            ps.setString(4, blog.getContent());
            ps.setString(5, blog.getCategory());
            ps.setInt(6, blogId);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Blog> getAllBlog() {
        List<Blog> list = new ArrayList<>();
        StatusDAO sd = new StatusDAOImpl();
        String query = "Select * from Blogs";
        try {
            con = new DBContext().connection;
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            Blog blog;
            while (rs.next()) {
                blog = new Blog();
                blog.setBlogId(rs.getInt(1));
                blog.setStatus(sd.getStatusById(rs.getInt(2)));
                blog.setTitle(rs.getString(3));
                blog.setImage(rs.getString(4));
                blog.setDate(rs.getDate(5));
                blog.setContent(rs.getString(6));
                blog.setCategory(rs.getString(7));
                list.add(blog);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    } 

    public static void main(String[] args) {
        BlogDAOImpl bd = new BlogDAOImpl();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//        bd.updateBlogById(1, new Blog(0, "Hello", "Hello.png", new Date(), "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non dui sodales, faucibus libero ut, posuere felis", "News"));
//        bd.changeBlogStatus(1);
//        System.out.println(bd.addBlog(new Blog(0, "Hello", "Hello.png", new Date(), "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non dui sodales, faucibus libero ut, posuere felis", "News", new Status(1, "ok"))));
    }

    @Override
    public List<Blog> getBlogsByName(String name) {
        List<Blog> list = new ArrayList<>();
        StatusDAO sd = new StatusDAOImpl();
        String query = "Select * from Blogs where [Title] like ?";
        try {
            con = new DBContext().connection;
            ps = con.prepareStatement(query);
            ps.setString(1, "%" + name + "%");
            rs = ps.executeQuery();
            Blog blog;
            while (rs.next()) {
                blog = new Blog();
                blog.setBlogId(rs.getInt(1));
                blog.setStatus(sd.getStatusById(rs.getInt(2)));
                blog.setTitle(rs.getString(3));
                blog.setImage(rs.getString(4));
                blog.setDate(rs.getDate(5));
                blog.setContent(rs.getString(6));
                blog.setCategory(rs.getString(7));
                list.add(blog);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }
}
