/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.edu.fpt.tourbooking.dao;

import java.util.List;
import vn.edu.fpt.tourbooking.entities.User;
/**
 *
 * @author Hoang Quoc Viet
 */
public interface UserDAO {
   User updateUser(User user);
   User getUserById(int userId);
   User getUserByEmail(String email);
   User insertUser(User user);
   User deleteUser(User user);
   List<User> getAllUsers();
}
