/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.edu.fpt.tourbooking.dao.impl;



import vn.edu.fpt.tourbooking.entities.Status;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import vn.edu.fpt.tourbooking.context.DBContext;
import vn.edu.fpt.tourbooking.dao.StatusDAO;

/**
 *
 * @author Admin
 */
public class StatusDAOImpl implements StatusDAO{
    Connection con = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    
    @Override
    public Status getStatusByType(String statusType) {
        Status status = new Status();
        String query = "Select * from Statuses where statusType = ?";
        try {
            con = new DBContext().connection;
            ps = con.prepareStatement(query);
            ps.setString(1, statusType);
            rs = ps.executeQuery();
            if (rs.next()) {
                status.setId(rs.getInt(1));
                status.setStatusType(rs.getString(2));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return status;
    }

    @Override
    public Status getStatusById(int statusId) {
        Status status = new Status();
        String query = "Select * from Statuses where ID = ?";
        try {
            con = new DBContext().connection;
            ps = con.prepareStatement(query);
            ps.setInt(1, statusId);
            rs = ps.executeQuery();
            if (rs.next()) {
                status.setId(rs.getInt(1));
                status.setStatusType(rs.getString(2));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return status;
    }
    
}

