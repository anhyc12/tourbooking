/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.edu.fpt.tourbooking.dao;

import vn.edu.fpt.tourbooking.entities.Booking;
import java.sql.Date;
import java.util.List;
/**
 *
 * @author Admin
 */
public interface BookingDAO{
    public List<Booking> getAllBookingByAccountID(int accountID);
    
    public List<Booking> getAllBookingByTourID(int tourID);
    
    public Booking getBookingByID(int bookingID);
    
    public Booking insertBooking(Booking booking);
    
    public Booking updateBooking(Booking booking);
    
    public Booking cancelBooking(Booking booking);
    
    public List<Booking> getAllBookingByDate(Date fromDate, Date toDate);
    
    public List<Booking> getAllBooking();  
}
