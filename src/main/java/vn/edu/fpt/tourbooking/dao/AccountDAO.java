/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.edu.fpt.tourbooking.dao;

import vn.edu.fpt.tourbooking.entities.Account;
import java.util.List;
/**
 *
 * @author Hoang Quoc Viet
 */
public interface AccountDAO {
   Account updateAccount(Account acount);
   Account getAccount(String username, String password);
   Account insertAccount(Account account);
   Account deleteAccount(Account account);
   List<Account> getAllAccounts();
   Account getAccountById(int id);
   Account getAccountByUsername(String username);
   int getStatisticAccount(int statusId);
}
