/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.edu.fpt.tourbooking.dao.impl;

import vn.edu.fpt.tourbooking.dao.BookingDAO;
import vn.edu.fpt.tourbooking.entities.Booking;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import vn.edu.fpt.tourbooking.context.DBContext;
import vn.edu.fpt.tourbooking.dao.AccountDAO;
import vn.edu.fpt.tourbooking.dao.StatusDAO;
import vn.edu.fpt.tourbooking.dao.TourDAO;

/**
 *
 * @author Admin
 */
public class BookingDAOImpl implements BookingDAO {

    Connection con = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    AccountDAO accountDAO = new AccountDAOImpl();
    TourDAO tourDAO = new TourDAOImpl();
    StatusDAO statusDAO = new StatusDAOImpl();
    
    //insert booking to database method
    @Override
    public Booking insertBooking(Booking booking) {
        //insert query
        String query = "insert into Bookings values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
        //return columns
        String returnColumns[] = {"ID"};
        try {
            //get database connection
            con = new DBContext().connection;
            //prepare statement
            ps = con.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
            //set params for query
            ps.setInt(1, booking.getTour().getId());
            ps.setInt(2, booking.getAccount().getId());
            ps.setInt(3, booking.getStatus().getId());
            ps.setDate(4, booking.getStartDate());
            ps.setDate(5, booking.getDueDate());
            ps.setInt(6, booking.getNumberPerson());
            ps.setDouble(7, booking.getPrice());
            ps.setString(8, booking.getLastName());
            ps.setString(9, booking.getFirstName());
            ps.setString(10, booking.getEmail());
            ps.setString(11, booking.getPhoneNumber());
            //execute update query
            int affectedRows = ps.executeUpdate();
            //check affected rows
            if (affectedRows == 1) {
                //get return columns
                rs = ps.getGeneratedKeys();
                //set return columns to booking object
                if (rs.next()) {
                    booking.setBookingID(rs.getInt(1));
                    return booking;
                } else {
                    throw new SQLException("Insert failed, no ID obtained.");
                }
            } else {
                throw new SQLException("Insert failed, no rows affected.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //get all booking from database method
    @Override
    public List<Booking> getAllBooking() {
        List<Booking> list = new ArrayList<>();
        //select all booking query
        String query = "Select * from Bookings";
        try {
            //get database connection
            con = new DBContext().connection;
            //prepare statement
            ps = con.prepareStatement(query);
            //execute query
            rs = ps.executeQuery();
            Booking booking;
            //set bookings data get from database to and add to return list
            while (rs.next()) {
                booking = new Booking();
                booking.setBookingID(rs.getInt("ID"));
                booking.setTour(tourDAO.getTourByID(rs.getInt("TourID")));
                booking.setAccount(accountDAO.getAccountById(rs.getInt("AccountID")));
                booking.setStatus(statusDAO.getStatusById(rs.getInt("StatusID")));
                booking.setStartDate(rs.getDate("StartDate"));
                booking.setDueDate(rs.getDate("DueDate"));
                booking.setNumberPerson(rs.getInt("PersonNumber"));
                booking.setPrice(rs.getDouble("Price"));
                booking.setLastName(rs.getString("LastName"));
                booking.setFirstName(rs.getString("FirstName"));
                booking.setEmail(rs.getString("Email"));
                booking.setPhoneNumber(rs.getString("PhoneNumber"));
                list.add(booking);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    //get all booking by account id from database method
    @Override
    public List<Booking> getAllBookingByAccountID(int accountID) {
        List<Booking> list = new ArrayList<>();
        //select all booking by account id query
        String query = "Select * from Bookings where AccountID= ?";
        try {
            //get database connection
            con = new DBContext().connection;
            //prepare statement
            ps = con.prepareStatement(query);
            //set account id param for query
            ps.setInt(1, accountID);
            //execute query
            rs = ps.executeQuery();
            Booking booking;
            //set bookings data get from database to and add to return list
            while (rs.next()) {
                booking = new Booking();
                booking.setBookingID(rs.getInt("ID"));
                booking.setTour(tourDAO.getTourByID(rs.getInt("TourID")));
                booking.setAccount(accountDAO.getAccountById(rs.getInt("AccountID")));
                booking.setStatus(statusDAO.getStatusById(rs.getInt("StatusID")));
                booking.setStartDate(rs.getDate("StartDate"));
                booking.setDueDate(rs.getDate("DueDate"));
                booking.setNumberPerson(rs.getInt("PersonNumber"));
                booking.setPrice(rs.getDouble("Price"));
                booking.setLastName(rs.getString("LastName"));
                booking.setFirstName(rs.getString("FirstName"));
                booking.setEmail(rs.getString("Email"));
                booking.setPhoneNumber(rs.getString("PhoneNumber"));
                list.add(booking);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }
    
    //get all booking by tour id from database method
    @Override
    public List<Booking> getAllBookingByTourID(int tourID) {
        List<Booking> list = new ArrayList<>();
        //select all booking by account id query
        String query = "Select * from Bookings where TourID= ?";
        try {
            //get database connection
            con = new DBContext().connection;
            //prepare statement
            ps = con.prepareStatement(query);
            //set tour id param for query
            ps.setInt(1, tourID);
            //execute query
            rs = ps.executeQuery();
            Booking booking;
            //set bookings data get from database to and add to return list
            while (rs.next()) {
                booking = new Booking();
                booking.setBookingID(rs.getInt("ID"));
                booking.setTour(tourDAO.getTourByID(rs.getInt("TourID")));
                booking.setAccount(accountDAO.getAccountById(rs.getInt("AccountID")));
                booking.setStatus(statusDAO.getStatusById(rs.getInt("StatusID")));
                booking.setStartDate(rs.getDate("StartDate"));
                booking.setDueDate(rs.getDate("DueDate"));
                booking.setNumberPerson(rs.getInt("PersonNumber"));
                booking.setPrice(rs.getDouble("Price"));
                booking.setLastName(rs.getString("LastName"));
                booking.setFirstName(rs.getString("FirstName"));
                booking.setEmail(rs.getString("Email"));
                booking.setPhoneNumber(rs.getString("PhoneNumber"));
                list.add(booking);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    //get booking by id from database method
    @Override
    public Booking getBookingByID(int bookingID) {
        Booking booking = null;
        //select booking by id query
        String query = "Select * from Bookings where ID = ?";
        try {
            //get database connection
            con = new DBContext().connection;
            //prepare statement
            ps = con.prepareStatement(query);
            //set id param for query
            ps.setInt(1, bookingID);
            //execute query
            rs = ps.executeQuery();
            //set data to return booking
            if (rs.next()) {
                booking = new Booking();
                booking.setBookingID(rs.getInt("ID"));
                booking.setTour(tourDAO.getTourByID(rs.getInt("TourID")));
                booking.setAccount(accountDAO.getAccountById(rs.getInt("AccountID")));
                booking.setStatus(statusDAO.getStatusById(rs.getInt("StatusID")));
                booking.setStartDate(rs.getDate("StartDate"));
                booking.setDueDate(rs.getDate("DueDate"));
                booking.setNumberPerson(rs.getInt("PersonNumber"));
                booking.setPrice(rs.getDouble("Price"));
                booking.setLastName(rs.getString("LastName"));
                booking.setFirstName(rs.getString("FirstName"));
                booking.setEmail(rs.getString("Email"));
                booking.setPhoneNumber(rs.getString("PhoneNumber"));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return booking;
    }

    //update booking in database method
    @Override
    public Booking updateBooking(Booking booking) {
        //update booking query
        String query = "Update Bookings set PersonNumber = ?, Price = ?, LastName = ?, FirstName = ?, Email = ?, PhoneNumber = ? where ID = ?";
        Booking b = new Booking();
        try {
            //get database connection
            con = new DBContext().connection;
            //prepare statement
            ps = con.prepareStatement(query);
            //set params for query
            ps.setInt(1, booking.getNumberPerson());
            ps.setDouble(2, booking.getPrice());
            ps.setString(3, booking.getLastName());
            ps.setString(4, booking.getFirstName());
            ps.setString(5, booking.getEmail());
            ps.setString(6, booking.getPhoneNumber());
            ps.setInt(7, booking.getBookingID());
            //execute update query
            int affectedRows = ps.executeUpdate();
            //check affected rows
            if (affectedRows == 1) {
                return booking;
            } else {
                throw new SQLException("Update failed, no rows affected.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //delete booking from database method
    @Override
    public Booking cancelBooking(Booking booking) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    //get all booking have start date in range from database method
    @Override
    public List<Booking> getAllBookingByDate(Date fromDate, Date toDate) {
        List<Booking> list = new ArrayList<>();
        //select all bookings have start date in range query
        String query = "select * from Bookings where StartDate between ? and ?";
        try{
            //get database connection
            con = new DBContext().connection;
            //prepare statement
            ps = con.prepareStatement(query);
            //set params for query
            ps.setDate(1, fromDate);
            ps.setDate(2, toDate);
            //execute query
            rs = ps.executeQuery();
            Booking booking;
            //set bookings data get from database to and add to return list
            while (rs.next()) {
                booking = new Booking();
                booking.setBookingID(rs.getInt("ID"));
                booking.setTour(tourDAO.getTourByID(rs.getInt("TourID")));
                booking.setAccount(accountDAO.getAccountById(rs.getInt("AccountID")));
                booking.setStatus(statusDAO.getStatusById(rs.getInt("StatusID")));
                booking.setStartDate(rs.getDate("StartDate"));
                booking.setDueDate(rs.getDate("DueDate"));
                booking.setNumberPerson(rs.getInt("PersonNumber"));
                booking.setPrice(rs.getDouble("Price"));
                booking.setLastName(rs.getString("LastName"));
                booking.setFirstName(rs.getString("FirstName"));
                booking.setEmail(rs.getString("Email"));
                booking.setPhoneNumber(rs.getString("PhoneNumber"));
                list.add(booking);
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return list;
    }
}
