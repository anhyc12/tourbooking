/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.edu.fpt.tourbooking.dao;

import vn.edu.fpt.tourbooking.entities.Booking;
import java.sql.Date;
import java.util.List;
import vn.edu.fpt.tourbooking.entities.Tour;

/**
 *
 * @author Admin
 */
public interface TourDAO {

    public List<Tour> getAllTour();

    public Tour getTourByID(int tourID);

    public Tour createTour(Tour tour);

    public Tour updateTour(Tour tour);

    public Tour deleteTour(int TourID);
    
    public List<Tour> getToursByName(String name);
}
