/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.edu.fpt.tourbooking.dao.impl;

import vn.edu.fpt.tourbooking.dao.TourDAO;
import vn.edu.fpt.tourbooking.entities.Tour;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import vn.edu.fpt.tourbooking.context.DBContext;
import vn.edu.fpt.tourbooking.dao.StatusDAO;
import vn.edu.fpt.tourbooking.entities.Status;

/**
 *
 * @author Admin
 */
public class TourDAOImpl implements TourDAO {

    Connection con = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    
    @Override
    public Tour createTour(Tour tour) {
        String query = "insert into  Tours values (?, ?, ?, ?, ?, ?, ?,?,?);";
        try {
            con = new DBContext().connection;
            ps = con.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setInt(1, tour.getStatus().getId());
            ps.setString(2, tour.getName());
            ps.setDouble(3, tour.getPrice());
            ps.setString(4, tour.getImage());
            ps.setString(5, tour.getDescription());
            ps.setString(6, tour.getCategory());
            ps.setString(7, tour.getLocation());
            ps.setString(8, tour.getStartDate());
            ps.setString(9, tour.getEndDate());
            
            ps.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    @Override
    public List<Tour> getAllTour() {
        List<Tour> list = new ArrayList<>();
        String query = "Select * from Tours";
        StatusDAO status = new StatusDAOImpl();
       
        try {
            con = new DBContext().connection;
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            Tour tour;
            StatusDAO s = new StatusDAOImpl();

            while (rs.next()) {
                tour = new Tour();
                tour.setId(rs.getInt(1));
                
                tour.setStatus(status.getStatusById(rs.getInt(2)));
                tour.setName(rs.getString(3));
                tour.setPrice(rs.getDouble(4));
                tour.setImage(rs.getString(5));
                tour.setCategory(rs.getString(6));
                tour.setDescription(rs.getString(7));
                tour.setLocation(rs.getString(8));
                tour.setStartDate(rs.getString(9));
                tour.setEndDate(rs.getString(10));
                list.add(tour);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    @Override
    public Tour getTourByID(int TourID) {
        Tour tour = new Tour();
        String query = "Select * from Tours where ID = ?";
        StatusDAO status = new StatusDAOImpl();
        try {
            con = new DBContext().connection;
            ps = con.prepareStatement(query);
            ps.setInt(1, TourID);
            rs = ps.executeQuery();
            if (rs.next()) {
                tour.setId(rs.getInt(1));
                tour.setStatus(status.getStatusById(rs.getInt(2)));
                tour.setName(rs.getString(3));
                tour.setPrice(rs.getDouble(4));
                tour.setImage(rs.getString(5));
                tour.setCategory(rs.getString(6));
                tour.setDescription(rs.getString(7));
                tour.setLocation(rs.getString(8));
                tour.setStartDate(rs.getString(9));
                tour.setEndDate(rs.getString(10));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return tour;
    }

    @Override
    public Tour updateTour(Tour tour) {
        String query = "Update Bookings set StartDate = ?, DueDate = ?, PersonNumber = ?, Price = ? where ID = ?";
        Tour b = new Tour();
        try {
            con = new DBContext().connection;
            ps = con.prepareStatement(query);
//            ps.setDate(1, booking.getStartDate());
//            ps.setDate(2, booking.getDueDate());
//            ps.setInt(3, booking.getNumberPerson());
//            ps.setDouble(4, booking.getPrice());
//            ps.setInt(5, booking.getBookingID());
            int affectedRows = ps.executeUpdate();
            if (affectedRows == 1) {
                return tour;
            } else {
                throw new SQLException("Update failed, no rows affected.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

 
   @Override
    public Tour deleteTour(int TourID) {
        String query = "DELETE FROM Tours"
                + "      WHERE ID=?";
        try {
            con = new DBContext().connection;
            ps = con.prepareStatement(query);
            ps.setInt(1, TourID);
            rs = ps.executeQuery();
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }
    
    public static void main(String[] args) throws SQLException {
        // test getAllProducts
        TourDAOImpl d = new TourDAOImpl();
        for (Tour s : d.getAllTour()) {
            System.out.println(s.getImage());
        }
//        System.out.print(d.getTourByID(1).getStatus().getStatusType());

//        System.out.print(d.getTourByID(1).getStatus);
        
//        d.createTour();
//          d.createTour(new Tour(new Status(1, "oke"), "abc", 124,"abc","abc","abc","abc","2002-11-11","2002-11-11"));
    }

    @Override
    public List<Tour> getToursByName(String name) {
        List<Tour> list = new ArrayList<>();
        String query = "Select * from Tours where [Name] like ?";
        StatusDAO status = new StatusDAOImpl();
       
        try {
            con = new DBContext().connection;
            ps = con.prepareStatement(query);
            ps.setString(1, "%" + name + "%");
            rs = ps.executeQuery();
            Tour tour;
            StatusDAO s = new StatusDAOImpl();

            while (rs.next()) {
                tour = new Tour();
                tour.setId(rs.getInt(1));
                
                tour.setStatus(status.getStatusById(rs.getInt(2)));
                tour.setName(rs.getString(3));
                tour.setPrice(rs.getDouble(4));
                tour.setImage(rs.getString(5));
                tour.setCategory(rs.getString(6));
                tour.setDescription(rs.getString(7));
                tour.setLocation(rs.getString(8));
                tour.setStartDate(rs.getString(9));
                tour.setEndDate(rs.getString(10));
                list.add(tour);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }
}
