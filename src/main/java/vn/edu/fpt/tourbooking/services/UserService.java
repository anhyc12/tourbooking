/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.edu.fpt.tourbooking.services;

import vn.edu.fpt.tourbooking.dto.BookingDTO;
import java.util.List;
import vn.edu.fpt.tourbooking.dto.UserDTO;
import vn.edu.fpt.tourbooking.entities.User;

/**
 *
 * @author Admin
 */
public interface UserService {
    List<UserDTO> getAllUsers();
    void createUser(UserDTO user);
    UserDTO getUserById(int userId);
    UserDTO getUserByEmail(String email);
    void updateUser(UserDTO user);
    void deleteUser(int user);
    
}
