/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.edu.fpt.tourbooking.services.impl;

import java.util.List;
import vn.edu.fpt.tourbooking.dao.UserDAO;
import vn.edu.fpt.tourbooking.dao.impl.UserDAOImpl;
import vn.edu.fpt.tourbooking.dto.UserDTO;
import vn.edu.fpt.tourbooking.entities.User;
import vn.edu.fpt.tourbooking.services.UserService;

/**
 *
 * @author Hoang Quoc Viet
 */
public class UserServiceImpl implements UserService{
    private UserDAO userDAO = new UserDAOImpl();
    
    private User convertToUser(UserDTO userDTO){
        if(userDTO == null) return null;
        User user = new User();
        user.setUserId(userDTO.getUserId());
        user.setAddress(userDTO.getAddress());
        user.setEmail(userDTO.getEmail());
        user.setFullName(userDTO.getFullName());
        user.setGender(userDTO.isGender());
        user.setPhoneNumber(userDTO.getPhoneNumber());
        return user;
    }
    
    private UserDTO convertToUserDTO(User user){
        if(user == null) return null;
        UserDTO userDTO = new UserDTO();
        userDTO.setUserId(user.getUserId());
        userDTO.setAddress(user.getAddress());
        userDTO.setEmail(user.getEmail());
        userDTO.setFullName(user.getFullName());
        userDTO.setGender(user.isGender());
        userDTO.setPhoneNumber(user.getPhoneNumber());
        return userDTO;
    }
    @Override
    public List<UserDTO> getAllUsers() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void createUser(UserDTO user) {
        userDAO.insertUser(convertToUser(user));
    }

    @Override
    public UserDTO getUserById(int userId) {
        return convertToUserDTO(userDAO.getUserById(userId));
    }

    @Override
    public UserDTO getUserByEmail(String email) {
        return convertToUserDTO(userDAO.getUserByEmail(email));
    }

    @Override
    public void updateUser(UserDTO user) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void deleteUser(int user) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    
    
}
