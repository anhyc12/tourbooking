/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.edu.fpt.tourbooking.services.impl;

import java.util.List;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import vn.edu.fpt.tourbooking.dao.AccountDAO;
import vn.edu.fpt.tourbooking.dao.StatusDAO;
import vn.edu.fpt.tourbooking.dao.impl.AccountDAOImpl;
import vn.edu.fpt.tourbooking.dao.impl.StatusDAOImpl;
import vn.edu.fpt.tourbooking.dto.AccountDTO;
import vn.edu.fpt.tourbooking.entities.Account;
import vn.edu.fpt.tourbooking.entities.Status;
import vn.edu.fpt.tourbooking.services.AccountService;

/**
 *
 * @author Hoang Quoc Viet
 */
public class AccountServiceImpl implements AccountService{
    private AccountDAO accountDAO = new AccountDAOImpl();
    private StatusDAO statusDAO = new StatusDAOImpl();
    private AccountDTO convertToAccountDTO(Account account){
        if(account == null) return null;
        return new AccountDTO(account.getId(), account.getUsername(), account.getPassword(), account.isRole(), account.getStatus(), account.getCode(), account.getExpiredTime());
    }
    private Account convertToAccount(AccountDTO account){
        if(account == null) return null;
        return new Account(account.getId(), account.getUsername(), account.getPassword(), account.isRole(), account.getStatus(), account.getCode(), account.getExpiredTime());
    }
    @Override
    public void createAccount(AccountDTO account) {
        accountDAO.insertAccount(convertToAccount(account));
    }

    @Override
    public AccountDTO getAccount(String username, String password) {
       Account account = accountDAO.getAccount(username, password);
       return convertToAccountDTO(account);
    }

    @Override
    public void setStatusAccount(int accountId, boolean status) {
        Account a = accountDAO.getAccountById(accountId);
        Status s = new Status();
        if(status){
            s = statusDAO.getStatusByType("actived");
        }
        else s = statusDAO.getStatusByType("non-actived");
        a.setStatus(s);
        accountDAO.updateAccount(a);
    }

    @Override
    public AccountDTO getAccountById(int id) {
        return convertToAccountDTO(accountDAO.getAccountById(id));
    }

    @Override
    public void updateAccount(AccountDTO account) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<AccountDTO> getAllAccount() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void sendConfirmMail(String toMail, String title, String content) {
        String username="vietpenta1312@gmail.com";
        String password="icuf sanx eprw dvfr";
        Properties properies = new Properties();
        properies.put("mail.smtp.host", "smtp.gmail.com");
        properies.put("mail.smtp.port", "465");
        properies.put("mail.smtp.auth", "true");
        properies.put("mail.smtp.starttls.enable", "true");
        properies.put("mail.smtp.starttls.required", "true");
        properies.put("mail.smtp.ssl.protocols", "TLSv1.2");
        properies.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        Session session = Session.getInstance(properies, new javax.mail.Authenticator(){
            @Override
            protected PasswordAuthentication getPasswordAuthentication(){
                return new PasswordAuthentication(username, password);
            }
        });
        try{
            Message message= new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toMail));
            message.setSubject(title);
            message.setContent(content, "text/HTML; charset=UTF-8");
            Transport.send(message);
            System.out.println("Send mail successfully");
        }
        catch(Exception ex){
            System.out.println(ex);
        }   
    }

    @Override
    public void deleteAccount(AccountDTO account) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int getStatisticAccount(int statusId) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
//    public static void main(String[] args) {
//        AccountService a = new AccountServiceImpl();
//        a.sendConfirmMail("quocviet13122002@gmail.com", "Activate Account", "Activate");
//    }

    @Override
    public AccountDTO getAccountByUsername(String username) {
        return convertToAccountDTO(accountDAO.getAccountByUsername(username));
    }
}
