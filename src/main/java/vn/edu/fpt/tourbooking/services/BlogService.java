/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.edu.fpt.tourbooking.services;

import java.util.List;
import vn.edu.fpt.tourbooking.dto.BlogDTO;

/**
 *
 * @author M.S.I
 */
public interface BlogService {
    public void changeBlogStatus(int blogId);
    public int addBlog(BlogDTO blog);
    public void updateBlogById(int blogId, BlogDTO blog);
    public List<BlogDTO> getAllBlog();
    public List<BlogDTO> getBlogsByName(String name);        
}
