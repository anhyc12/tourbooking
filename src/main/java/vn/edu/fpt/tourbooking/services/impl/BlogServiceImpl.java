/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.edu.fpt.tourbooking.services.impl;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import vn.edu.fpt.tourbooking.dao.BlogDAO;
import vn.edu.fpt.tourbooking.dao.StatusDAO;
import vn.edu.fpt.tourbooking.dao.impl.BlogDAOImpl;
import vn.edu.fpt.tourbooking.dao.impl.StatusDAOImpl;
import vn.edu.fpt.tourbooking.dto.BlogDTO;
import vn.edu.fpt.tourbooking.entities.Blog;
import vn.edu.fpt.tourbooking.services.BlogService;

/**
 *
 * @author M.S.I
 */
public class BlogServiceImpl implements BlogService{

    private final BlogDAO blogDAO = new BlogDAOImpl();
    private final StatusDAO statusDAO = new StatusDAOImpl();
    private BlogDTO convertToBlogDTO(Blog blog){
        return new BlogDTO(
                blog.getBlogId(), 
                blog.getTitle(), 
                blog.getImage(), 
                blog.getDate(),
                blog.getContent(), 
                blog.getCategory(), 
                blog.getStatus().getStatusType()
        );
    }
    
    private Blog convertToBlog(BlogDTO blogDTO){
        return new Blog(
                blogDTO.getBlogId(),
                blogDTO.getTitle(),
                blogDTO.getImage(),
                blogDTO.getDate(),
                blogDTO.getContent(),
                blogDTO.getCategory(),
                statusDAO.getStatusByType(blogDTO.getStatusId())
        );
    }
    @Override
    public void changeBlogStatus(int blogId) {
        blogDAO.changeBlogStatus(blogId);
    }

    @Override
    public int addBlog(BlogDTO blog) {
        return blogDAO.addBlog(convertToBlog(blog));
    }

    @Override
    public void updateBlogById(int blogId, BlogDTO blog) {
        blogDAO.updateBlogById(blogId, convertToBlog(blog));
    }

    @Override
    public List<BlogDTO> getAllBlog() {
        List<Blog> list = blogDAO.getAllBlog();
        return list.stream().map(n -> convertToBlogDTO(n)).collect(Collectors.toList());
    }

    @Override
    public List<BlogDTO> getBlogsByName(String name) {
        List<Blog> list = blogDAO.getBlogsByName(name);
        return list.stream().map(n -> convertToBlogDTO(n)).collect(Collectors.toList());
    }
    public static void main(String[] args) {
        BlogService bs = new BlogServiceImpl();
//        for(BlogDTO blog : bs.getAllBlog()){
//            System.out.println(blog.getStatusId());
//        }
        
        System.out.println(bs.addBlog(new BlogDTO(0, "Hello", "Hello.png", new Date(), "Hi", "News", "ok")));
    }
}
