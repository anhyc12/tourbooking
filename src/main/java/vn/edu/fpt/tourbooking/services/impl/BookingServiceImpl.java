/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.edu.fpt.tourbooking.services.impl;




import vn.edu.fpt.tourbooking.dao.BookingDAO;
import vn.edu.fpt.tourbooking.entities.Booking;
import java.sql.Date;
import java.util.List;

import vn.edu.fpt.tourbooking.dao.StatusDAO;
import vn.edu.fpt.tourbooking.dao.impl.BookingDAOImpl;
import vn.edu.fpt.tourbooking.dao.impl.StatusDAOImpl;

import vn.edu.fpt.tourbooking.dto.BookingDTO;


import vn.edu.fpt.tourbooking.services.BookingService;
import java.util.stream.Collectors;
import vn.edu.fpt.tourbooking.dao.AccountDAO;
import vn.edu.fpt.tourbooking.dao.TourDAO;
import vn.edu.fpt.tourbooking.dao.impl.AccountDAOImpl;
import vn.edu.fpt.tourbooking.dao.impl.TourDAOImpl;




/**
 *
 * @author Admin
 */
public class BookingServiceImpl implements BookingService{
    private final BookingDAO bookingDAO = new BookingDAOImpl();
    private final StatusDAO statusDAO = new StatusDAOImpl();
    private final AccountDAO accountDAO = new AccountDAOImpl();
    private final TourDAO tourDAO = new TourDAOImpl();

    

    private BookingDTO convertToBookingDTO(Booking booking){
        return new BookingDTO(
                booking.getBookingID(), 
                booking.getStartDate(), 
                booking.getDueDate(), 
                booking.getNumberPerson(),
                booking.getPrice(), 
                booking.getAccount().getId(), 
                booking.getTour().getId(),
                booking.getStatus().getStatusType(),
                booking.getLastName(),
                booking.getFirstName(),
                booking.getEmail(),
                booking.getPhoneNumber()
        );
    }
    
    private Booking convertToBooking(BookingDTO bookingDTO){
        return new Booking(
                bookingDTO.getBookingID(),
                bookingDTO.getStartDate(),
                bookingDTO.getDueDate(),
                bookingDTO.getNumberPerson(),
                bookingDTO.getPrice(),
                accountDAO.getAccountById(bookingDTO.getAccountID()),
                tourDAO.getTourByID(bookingDTO.getTourID()),
                statusDAO.getStatusByType(bookingDTO.getStatusType()),
                bookingDTO.getLastName(),
                bookingDTO.getFirstName(),
                bookingDTO.getEmail(),
                bookingDTO.getPhoneNumber()
        );

    }
    
    @Override
    public List<BookingDTO> getAllBookingByAccountID(int accountID) {
        List<Booking> list = bookingDAO.getAllBookingByAccountID(accountID);
        if (list.isEmpty()){
            return null;
        }
        return list.stream().map(n -> convertToBookingDTO(n)).collect(Collectors.toList());
    }


    @Override
    public List<BookingDTO> getAllBookingByTourID(int tourID) {
        List<Booking> list = bookingDAO.getAllBookingByTourID(tourID);
        if (list.isEmpty()){
            return null;
        }
        return list.stream().map(n -> convertToBookingDTO(n)).collect(Collectors.toList());
    }
    
    
    @Override
    public BookingDTO getBookingByID(int bookingID) {
        Booking booking = bookingDAO.getBookingByID(bookingID);
        if (booking == null){
            return null;
        }
        return convertToBookingDTO(booking);
    }


    @Override
    public BookingDTO insertBooking(BookingDTO booking) {
        Booking b = bookingDAO.insertBooking(convertToBooking(booking));
        if (booking == null){
            return null;
        }
        return convertToBookingDTO(b);
    }

    @Override
    public BookingDTO updateBooking(BookingDTO booking) {
        Booking b = bookingDAO.updateBooking(convertToBooking(booking));
        if (booking == null){
            return null;
        }
        return convertToBookingDTO(b);
    }

    @Override
    public BookingDTO cancelBooking(BookingDTO booking) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<BookingDTO> getAllBookingByDate(Date fromDate, Date toDate) {
        List<Booking> list = bookingDAO.getAllBookingByDate(fromDate, toDate);
        if (list.isEmpty()){
            return null;
        }
        return list.stream().map(n -> convertToBookingDTO(n)).collect(Collectors.toList());
    }

    @Override
    public List<BookingDTO> getAllBooking() {
        List<Booking> list = bookingDAO.getAllBooking();
        if (list.isEmpty()){
            return null;
        }
        return list.stream().map(n -> convertToBookingDTO(n)).collect(Collectors.toList());
    }  
}
