/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.edu.fpt.tourbooking.services;

import java.util.List;
import vn.edu.fpt.tourbooking.dto.AccountDTO;

/**
 *
 * @author Admin
 */
public interface AccountService {
    void createAccount(AccountDTO account);
    AccountDTO getAccount(String username, String password);
    void setStatusAccount(int accountId, boolean status);
    AccountDTO getAccountById(int id);
    AccountDTO getAccountByUsername(String username);
   void updateAccount(AccountDTO account);
   List<AccountDTO> getAllAccount();
   void sendConfirmMail(String toMail, String title, String content);
   void deleteAccount(AccountDTO account);
   int getStatisticAccount(int statusId);
   
}
