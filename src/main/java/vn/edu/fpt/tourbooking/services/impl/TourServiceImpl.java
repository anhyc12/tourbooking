/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.edu.fpt.tourbooking.services.impl;

import java.sql.Date;
import java.sql.SQLException;
import java.util.List;
import vn.edu.fpt.tourbooking.dao.StatusDAO;
import vn.edu.fpt.tourbooking.dao.impl.StatusDAOImpl;

import java.util.stream.Collectors;
import vn.edu.fpt.tourbooking.dao.TourDAO;
import vn.edu.fpt.tourbooking.dao.impl.TourDAOImpl;

import vn.edu.fpt.tourbooking.dto.TourDTO;
import vn.edu.fpt.tourbooking.entities.Tour;

import vn.edu.fpt.tourbooking.services.TourService;

/**
 *
 * @author Admin
 */
public class TourServiceImpl implements TourService {

    private final StatusDAO statusDAO = new StatusDAOImpl();
    private final TourDAO tourDAO = new TourDAOImpl();

    private TourDTO convertToTourDTO(Tour tour) {
        return new TourDTO(
                tour.getId(),
                tour.getName(),
                tour.getPrice(),
                tour.getImage(),
                tour.getCategory(),
                tour.getLocation(),
                tour.getDescription(),
                tour.getStartDate(),
                tour.getEndDate(),
                tour.getStatus().getStatusType()
        );
    }

    private Tour convertToTour(TourDTO tourDTO) {
        return new Tour(
                tourDTO.getId(),
                tourDTO.getName(),
                tourDTO.getPrice(),
                tourDTO.getImage(),
                tourDTO.getCategory(),
                tourDTO.getLocation(),
                tourDTO.getDescription(),
                tourDTO.getStartDate(),
                tourDTO.getEndDate(),
                statusDAO.getStatusByType(tourDTO.getStatusType())
        );
    }

    @Override
    public List<TourDTO> getAllTour() {
        List<Tour> list = tourDAO.getAllTour();
        return list.stream().map(n -> convertToTourDTO(n)).collect(Collectors.toList());
    }

    @Override
    public TourDTO getTourByID(int tourID) {
        Tour tour = tourDAO.getTourByID(tourID);
        return convertToTourDTO(tour);
    }

    @Override
    public TourDTO createTour(TourDTO tour) {
        tourDAO.createTour(convertToTour(tour));
        return null;    
    }

    @Override
    public TourDTO updateTour(Tour tour) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public TourDTO deleteTour(int TourID) {
        Tour tour = tourDAO.deleteTour(TourID);
        return null;
    }    

    @Override
    public List<TourDTO> getToursByName(String name) {
        List<Tour> list = tourDAO.getToursByName(name);
        return list.stream().map(n -> convertToTourDTO(n)).collect(Collectors.toList());
    }
    
        public static void main(String[] args) throws SQLException {
        // test getAllProducts
        TourService t = new TourServiceImpl();
        t.createTour(new TourDTO("ok", "11111", 100, "ttttt", "ttttt", "ttttt", "ttttt","2023-11-30", "11-11-2002"));
//        for (TourDTO s : t.getAllTour()) {
//            System.out.println(s.getStatusType());
//        }

//        System.out.print(d.getTourByID(1).getStatus);
    }

}
