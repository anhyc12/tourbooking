/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.edu.fpt.tourbooking.services;

import java.util.List;
import vn.edu.fpt.tourbooking.dto.TourDTO;
import vn.edu.fpt.tourbooking.entities.Tour;

/**
 *
 * @author M.S.I
 */
public interface TourService {

    public List<TourDTO> getAllTour();

    public TourDTO getTourByID(int tourID);

    public TourDTO createTour(TourDTO tour);

    public TourDTO updateTour(Tour tour);

    public TourDTO deleteTour(int TourID);
    
    public List<TourDTO> getToursByName(String name);
}
