/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.edu.fpt.tourbooking.services;


import vn.edu.fpt.tourbooking.dto.BookingDTO;
import java.sql.Date;

import java.util.List;

/**
 *
 * @author Admin
 */
public interface BookingService {
    public List<BookingDTO> getAllBookingByAccountID(int accountID);
    
    public List<BookingDTO> getAllBookingByTourID(int tourID);
    
    public BookingDTO getBookingByID(int bookingID);
    
    public BookingDTO insertBooking(BookingDTO booking);
    
    public BookingDTO updateBooking(BookingDTO booking);
    
    public BookingDTO cancelBooking(BookingDTO booking);
    
    public List<BookingDTO> getAllBookingByDate(Date fromDate, Date toDate);
    
    public List<BookingDTO> getAllBooking();
}
