/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.edu.fpt.tourbooking.dto;

import java.util.Date;

/**
 *
 * @author M.S.I
 */
public class BlogDTO {
    private int blogId;
    private String title;
    private String image;
    private Date date;
    private String content;
    private String category;
    private String status;

    public BlogDTO() {
    }

    public BlogDTO(int blogId, String title, String image, Date date, String content, String category, String statusId) {
        this.blogId = blogId;
        this.title = title;
        this.image = image;
        this.date = date;
        this.content = content;
        this.category = category;
        this.status = statusId;
    }
    
    
    
    public int getBlogId() {
        return blogId;
    }

    public void setBlogId(int blogId) {
        this.blogId = blogId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getStatusId() {
        return status;
    }

    public void setStatusId(String statusId) {
        this.status = statusId;
    }
    
}
