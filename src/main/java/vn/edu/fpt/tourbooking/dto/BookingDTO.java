/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.edu.fpt.tourbooking.dto;

import java.sql.Date;

/**
 *
 * @author Admin
 */
public class BookingDTO {
    //properties
    private int bookingID;
    private Date startDate;
    private Date dueDate;
    private int numberPerson;
    private double price;
    private int accountID;
    private int tourID;
    private String statusType;
    private String lastName;
    private String firstName;
    private String email;
    private String phoneNumber;

    //no args constructer
    public BookingDTO() {
    }

    //all args constructor
    public BookingDTO(int bookingID, Date startDate, Date dueDate, int numberPerson, double price, int accountID, int tourID, String statusType, String lastName, String firstName, String email, String phoneNumber) {
        this.bookingID = bookingID;
        this.startDate = startDate;
        this.dueDate = dueDate;
        this.numberPerson = numberPerson;
        this.price = price;
        this.accountID = accountID;
        this.tourID = tourID;
        this.statusType = statusType;
        this.lastName = lastName;
        this.firstName = firstName;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }

    //bookingID getter
    public int getBookingID() {
        return bookingID;
    }
    
    //bookingID setter
    public void setBookingID(int bookingID) {
        this.bookingID = bookingID;
    }

    //startDate getter
    public Date getStartDate() {
        return startDate;
    }

    //startDate setter
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    
    //dueDate getter
    public Date getDueDate() {
        return dueDate;
    }

    //dueDate setter
    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    //numberPerson getter
    public int getNumberPerson() {
        return numberPerson;
    }

    //numberPerson setter
    public void setNumberPerson(int numberPerson) {
        this.numberPerson = numberPerson;
    }

    //price getter
    public double getPrice() {
        return price;
    }
    
    //price setter
    public void setPrice(double price) {
        this.price = price;
    }

    //accountID getter
    public int getAccountID() {
        return accountID;
    }

    //accountID setter
    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }

    //toutID getter
    public int getTourID() {
        return tourID;
    }

    //tourId setter
    public void setTourID(int tourID) {
        this.tourID = tourID;
    }

    //statusType getter
    public String getStatusType() {
        return statusType;
    }

    //statusType setter
    public void setStatusType(String statusType) {
        this.statusType = statusType;
    }

    //lastName getter
    public String getLastName() {
        return lastName;
    }

    //lastName setter
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    //firstName getter
    public String getFirstName() {
        return firstName;
    }

    //firstName setter
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    //email getter
    public String getEmail() {
        return email;
    }

    //email setter
    public void setEmail(String email) {
        this.email = email;
    }

    //phoneNumber getter
    public String getPhoneNumber() {
        return phoneNumber;
    }

    //phoneNumber setter
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    
    
}
