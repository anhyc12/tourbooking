/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.edu.fpt.tourbooking.dto;

/**
 *
 * @author Admin
 */
public class TourDTO {

    private int Id;
    private String name;
    private double price;
    private String image;
    private String category;
    private String description;
    private String location;
    private String StartDate;
    private String EndDate;
    private String statusType;

    public TourDTO() {
    }

    public TourDTO(String statusType, String name, double price, String image, String category, String description, String location, String StartDate, String EndDate) {
        this.statusType = statusType;
        this.name = name;
        this.price = price;
        this.image = image;
        this.category = category;
        this.description = description;
        this.location = location;
        this.StartDate = StartDate;
        this.EndDate = EndDate;
    }

    public TourDTO(int Id, String name, double price, String image, String category, String description, String location, String StartDate, String EndDate, String statusType) {
        this.Id = Id;
        this.name = name;
        this.price = price;
        this.image = image;
        this.category = category;
        this.description = description;
        this.location = location;
        this.StartDate = StartDate;
        this.EndDate = EndDate;
        this.statusType = statusType;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String StartDate) {
        this.StartDate = StartDate;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String EndDate) {
        this.EndDate = EndDate;
    }

    public String getStatusType() {
        return statusType;
    }

    public void setStatus(String statusType) {
        this.statusType = statusType;
    }

}
