/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.edu.fpt.tourbooking.dto;

import java.sql.Date;
import java.sql.Timestamp;
import vn.edu.fpt.tourbooking.entities.Status;

/**
 *
 * @author Hoang Quoc Viet
 */
public class AccountDTO {
    private int id;
    private String username;
    private String password;
    private boolean role;
    private Status status;
    private String code;
    private Timestamp expiredTime;
    public AccountDTO() {
    }

    public AccountDTO(String username, String password, String code, Timestamp expiredTime) {
        this.username = username;
        this.password = password;
        this.code = code;
        this.expiredTime = expiredTime;
        this.role = false;
    }
    
    public AccountDTO(int id, String username, String password, boolean role, Status status, String code, Timestamp expiredTime) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.role = role;
        this.status = status;
        this.code = code;
        this.expiredTime = expiredTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
 
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isRole() {
        return role;
    }

    public void setRole(boolean role) {
        this.role = role;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Timestamp getExpiredTime() {
        return expiredTime;
    }

    public void setExpiredTime(Timestamp expiredTime) {
        this.expiredTime = expiredTime;
    }
}
