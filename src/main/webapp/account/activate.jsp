<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">        
        <title>Confirm account</title>
        <link rel="stylesheet" href="css/login_style.css">
        <jsp:include page="../layouts/head_assets.jsp"/>
    </head>
    <body>
        <div class="site-header">
        <div class="container">
            <div class="main-header">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-10">
                        <div class="logo">
                            <a href="/TourBooking/home">
                                <img src="images/logo.png" alt="travel html5 template" title="travel html5 template">
                            </a>
                        </div> <!-- /.logo -->
                    </div> <!-- /.col-md-4 -->
                  
                </div> <!-- /.row -->
            </div> <!-- /.main-header -->
         
        </div> <!-- /.container -->
    </div> <!-- /.site-header -->
        
        <div id="content" class="container">
            <h2 class="fs-2">Activation account</h2>
            <p class="fs-4 mt-2 text-danger">${requestScope.msg}</p>
            <c:if test="${requestScope.status == null}">
                <form class="mt-3" method="post" action="/confirm-account">
                    <button class="btn btn-primary" id="registerBtn" type="button">Resend active link </button>
                </form>
            </c:if>
        </div>
    
        <jsp:include page="../layouts/footer.jsp"/>
        
        <jsp:include page="../layouts/body_assets.jsp"/>
    </body>
</html>
