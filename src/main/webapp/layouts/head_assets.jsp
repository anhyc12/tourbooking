<%-- 
    Document   : header_assets
    Created on : 22-Oct-2023, 19:06:24
    Author     : anhyc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.css">
<link rel="stylesheet" href="css/animate.css">
<link rel="stylesheet" href="css/templatemo_misc.css">
<link rel="stylesheet" href="css/templatemo_style.css">
<link rel="stylesheet" href="css/style.css">

<script src="js/vendor/modernizr-2.6.1-respond-1.1.0.min.js"></script>