<%-- 
    Document   : body_assets
    Created on : 22-Oct-2023, 19:06:43
    Author     : anhyc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<script src="js/vendor/jquery-1.11.0.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.0.min.js"><\/script>')</script>
<script src="js/bootstrap.js"></script>
<script src="js/plugins.js"></script>
<script src="js/main.js"></script>
<script>
    var loginBtn = document.getElementById("loginBtn");
    if (loginBtn) {
    loginBtn.addEventListener("click", function() {
        window.location.href = "/TourBooking/login";
    });
    } 
    
    var logoutBtn = document.getElementById("logoutButton");
    if(logoutBtn){
        logoutBtn.addEventListener("click", function() {
        window.location.href = "/TourBooking/logout";
    });
    }
    
    var registerBtn = document.getElementById("registerBtn");
    if(registerBtn){
         registerBtn.addEventListener("click", function() {
             window.location.href = "/TourBooking/register";
    });
    }
</script>