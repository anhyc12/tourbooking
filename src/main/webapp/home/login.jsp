<%-- 
    Document   : index
    Created on : 22-Oct-2023, 18:52:34
    Author     : anhyc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">        
        <title>Login</title>
        <link rel="stylesheet" href="css/login_style.css">
        <jsp:include page="../layouts/head_assets.jsp"/>
    </head>
    <body>
        <div class="site-header">
        <div class="container">
            <div class="main-header">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-10">
                        <div class="logo">
                            <a href="home">
                                <img src="images/logo.png" alt="travel html5 template" title="travel html5 template">
                            </a>
                        </div> <!-- /.logo -->
                    </div> <!-- /.col-md-4 -->
                  
                </div> <!-- /.row -->
            </div> <!-- /.main-header -->
         
        </div> <!-- /.container -->
    </div> <!-- /.site-header -->
        
        <div id="content" class="container">
            <div class="form_container">
                <h3>Login</h3>
                <form action="login" id='form_register' method="post">
                    <span>${requestScope.error}</span>
                    <input placeholder="Username" type="text" name="usernameInput" class="form-control">
                    <input placeholder="Password" type="password" name="pwdInput" class="form-control">
                    <div class="forgot_div"><a href="#">Forgot password ? </a></div>
                    
                    <button type="submit">Login </button>
                </form>
            </div>
        </div>
        
        <jsp:include page="../layouts/footer.jsp"/>
        
        <jsp:include page="../layouts/body_assets.jsp"/>
    </body>
</html>
