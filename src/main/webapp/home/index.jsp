<%-- 
    Document   : index
    Created on : 22-Oct-2023, 18:52:34
    Author     : anhyc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">        
        <title>Index page</title>
        
        <jsp:include page="../layouts/head_assets.jsp"/>
    </head>
    <body>
        <jsp:include page="../layouts/header.jsp"/>
        
        <div id="content">
            <!-- điền nội dung của ae vào đây khi tạo file mới nhé -->
            <!-- nhớ phải có folder chứa nó (ví dụ file này là folder 'home') không thì sẽ dẫn đến lỗi đường dẫn -->
        </div>
        
        <jsp:include page="../layouts/footer.jsp"/>
        
        <jsp:include page="../layouts/body_assets.jsp"/>
        
    </body>
</html>
