<%-- 
    Document   : index
    Created on : 22-Oct-2023, 18:52:34
    Author     : anhyc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">        
        <title>Register</title>
        <link rel="stylesheet" href="css/register_style.css">
        <jsp:include page="../layouts/head_assets.jsp"/>
    </head>
    <body>
        <div class="site-header">
        <div class="container">
            <div class="main-header">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-10">
                        <div class="logo">
                            <a href="home">
                                <img src="images/logo.png" alt="travel html5 template" title="travel html5 template">
                            </a>
                        </div> <!-- /.logo -->
                    </div> <!-- /.col-md-4 -->
                  
                </div> <!-- /.row -->
            </div> <!-- /.main-header -->
         
        </div> <!-- /.container -->
    </div> <!-- /.site-header -->
        
        <div id="content" class="container">
            <div class="form_container">
                <h3>Register account</h3>
                <form action="register" id='form_register' method="post">
                    <input placeholder="Username" type="text" name="usernameInput" class="form-control">
                    <span class="text-danger">${requestScope.usernameError}</span>
                    <input placeholder="Password" type="password" name="passwordInput" class="form-control">
                    <input placeholder="Confirm password" type="password" name="confirmPwdInput" class="form-control">
                    <input placeholder="Email Address" type="email" name="emailInput" class="form-control">
                    <span class="text-danger">${requestScope.emailError}</span>
                    <button type="submit">Register </button>
                </form>
            </div>
        </div>
        
        <jsp:include page="../layouts/footer.jsp"/>
        
        <jsp:include page="../layouts/body_assets.jsp"/>
        <script>
    document.addEventListener('DOMContentLoaded', function() {
        document.getElementById('form_register').addEventListener('submit', function(event) {
            var username = document.getElementsByName('usernameInput')[0].value;
            var password = document.getElementsByName('passwordInput')[0].value;
            var confirmPwd = document.getElementsByName('confirmPwdInput')[0].value;

            var passwordRegex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W_]).{8,32}$/;
            var usernameRegex = /^[a-zA-Z0-9_-]{4,32}$/;

            if (!usernameRegex.test(username)) {
                alert('Username must be between 4 and 32 characters and may only contain letters, numbers, underscore (_) and hyphen (-).');
                event.preventDefault();
            }
            else if (!passwordRegex.test(password)) {
                alert('Password must be between 8 and 32 characters and include at least one digit, one upper case letter, one lower case letter, and one special character.');
                event.preventDefault();
            }
            else if (password !== confirmPwd) {
                alert('Passwords do not match.');
                event.preventDefault();
            }
        });
    });
</script>

    </body>
</html>
