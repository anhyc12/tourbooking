<%-- 
    Document   : View
    Created on : Oct 30, 2023, 1:30:02 PM
    Author     : Admin
--%>
<%@page import="vn.edu.fpt.tourbooking.dto.TourDTO"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <jsp:include page="../layouts/head_assets.jsp"/>
        <style>
            .card-img-top {
                width: 150px;
                height: 150px;
            }
            .list{
                min-height: 500px;
            }
            .title{
                margin-left: 110px;
                font-size: 32px;
            }
            .card-title{
                font-size: 18px;
            }
        </style>
    </head>
    <body>
        <jsp:include page="../layouts/header.jsp"/>
        <div id="content" style="margin-top: 250px">
            <div class="container">
                <h1 class="title">Booking List</h1>
                <div class="row justify-content-center mt-4">
                    <div class="col-md-10 w-75">
                        <div class="d-flex justify-content-end">
                            <form method="get" class="d-flex mb-4 w-50">
                                <label class="me-2" for="fromDate">From:</label>
                                <input type="date" name="from" id="fromDate" class="form-control me-2" />
                                <label class="me-2" for="toDate">To:</label>
                                <input type="date" name="to" id="toDate" class="form-control me-2" />
                                <button type="submit" class="btn btn-primary h-75">Search</button>
                            </form>
                        </div>
                        <div class="list">
                            <c:forEach var="booking" items="${requestScope.listBooking}">
                                <div class="card flex-row mb-2">
                                    <div class="card-body">
                                        <div>
                                            <h5 class="card-title">Guest Name: ${booking.getLastName()} ${booking.getFirstName()}</h5>
                                            <p class="card-text">Email: ${booking.getEmail()}</p>
                                            <p class="card-text">Status: ${booking.getPhoneNumber()}</p>
                                            <p class="card-text">Status: ${booking.getStatusType()}</p>
                                        </div>
                                        <div class="d-flex justify-content-end">
                                            <button class="btn btn-primary me-2" onclick="update(${booking.getBookingID()})">Update</button>
                                            <button class="btn btn-danger">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                </div>
            </div>
        </div>   
        <script>
            if (<%= request.getAttribute("listBooking") %> == null){
                alert('no list data');
            }
        </script>
        <script>
            var update = function (id) {
                window.location.href = "UpdateBookingServlet?id=" + id;
            }
        </script>
        <jsp:include page="../layouts/footer.jsp"/>

        <jsp:include page="../layouts/body_assets.jsp"/>
    </body>
</html>
