<%-- 
    Document   : update
    Created on : Nov 2, 2023, 1:11:37 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <jsp:include page="../layouts/head_assets.jsp"/>
        <script src="https://www.paypal.com/sdk/js?client-id=AUWhI4daXRzzzPuCLHcmnB7VhWvUd_RSJ0FQXeoOgAC7TQwGClUvVkVTIh4O868Uje6Ql2_uSbWkTxNE"></script>
    </head>
    <body>
        <jsp:include page="../layouts/header.jsp"/>
        <div id="content" style="margin: 15%; margin-top: 250px">
            <div class="container-fluid justify-content-center">
                <main class="row">
                    <section class="col-8 d-flex gap-3 flex-column">
                        <form method="post">
                            <!-- 1 -->
                            <div class="border border-2 p-4 border-dark" style="background-color: #f5f5f5">
                                <div class="d-flex gap-2">
                                    <div
                                        class="d-flex align-items-center justify-content-center"
                                        style="width: 35px; height: 35px; background-color: #091747"
                                        >
                                        1
                                    </div>
                                    <h3 style="font-size: 18px; font-weight: bold">Add user detail</h3>
                                </div>
                                <p>Leader traveler</p>
                                <div class="mb-3">
                                    <label class="form-label">First name</label>
                                    <input name="inpFirstName" type="text" class="form-control" value="${requestScope.bookingDTO.getFirstName()}" required="true"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Last name</label>
                                    <input name="inpLastName" type="text" class="form-control" value="${requestScope.bookingDTO.getLastName()}" required="true"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Email</label>
                                    <input name="inpEmail" type="email" class="form-control" value="${requestScope.bookingDTO.getEmail()}" required="true"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-check-label">Phone number</label>
                                    <input name="inpPhoneNumber" type="text" class="form-control" value="${requestScope.bookingDTO.getPhoneNumber()}" required="true"/>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Traveler</label>
                                    <input name="inpNumberPerson" id="inpNumberPerson" type="number" value="${requestScope.bookingDTO.getNumberPerson()}" oninput="change(${requestScope.tourDTO.getPrice()})" required="true" class="form-control" />
                                </div>
                                <input name="inpPrice" id="inpPrice" value="${requestScope.bookingDTO.getPrice()}" type="hidden"/>
                            </div>

                            <!-- 2 -->
                            <div class="border border-2 p-4 border-dark mt-3" style="background-color: #f5f5f5">
                                <div class="d-flex gap-2">
                                    <div
                                        class="d-flex align-items-center justify-content-center"
                                        style="width: 35px; height: 35px; background-color: #091747"
                                        >
                                        2
                                    </div>
                                    <h3 style="font-size: 18px; font-weight: bold">Payment</h3>
                                </div>
                                <p>Click here to sign into your Paypal Account</p>
                                <div id="paypal-button-container"></div>
                            </div>

                            <!-- btns -->
                            <div class="d-flex gap-3 mt-3">
                                <button name="btnSubmit" id="btnSubmit" class="btn btn-primary w-full" type="submit" disabled="true">Submit</button>
                                <button name="btnCancel" type="button" class="btn btn-danger" onclick="cancel()">Cancel</button>
                            </div>
                        </form>
                    </section>

                    <section class="col-4 d-flex flex-column gap-3">
                        <!-- my trip -->
                        <div class="border border-2 p-4 border-dark" style="background-color: #f5f5f5">
                            <h3 style="font-size: 18px; font-weight: bold">My trip</h3>
                            <div>
                                <p>Tour name</p>
                                <p>${requestScope.tourDTO.getName()}</p>
                            </div>
                            <div>
                                <p>Start date</p>
                                <p>${requestScope.tourDTO.getStartDate().toString()}</p>
                            </div>
                            <div>
                                <p>End date</p>
                                <p>${requestScope.tourDTO.getEndDate().toString()}</p>
                            </div>
                            <div>
                                <p>Location</p>
                                <p>${requestScope.tourDTO.getLocation()}</p>
                            </div>
                        </div>

                        <!-- cost -->
                        <div class="border border-2 p-4 border-dark" style="background-color: #f5f5f5">
                            <h3 style="font-size: 18px; font-weight: bold">Cost</h3>
                            <input id="price" value="${requestScope.bookingDTO.getPrice()}" readonly="true"> VND
                        </div>
                    </section>
                </main>
            </div>
        </div>
        <script>

            var cancel = function () {
                window.location.href = "ViewBookingServlet";
            }
            var change = function (tourPrice) {
                const a = document.getElementById("inpNumberPerson").value;
                document.getElementById("price").value = (tourPrice * a * 5) / 100;
                document.getElementById("inpPrice").value = (tourPrice * a * 5) / 100;
            }
        </script>
        <script>
            paypal.Buttons({
                createOrder: function (data, actions) {
                    // Set up the transaction
                    return actions.order.create({
                        purchase_units: [{
                                amount: {
                                    value: '10.00' // Replace with the amount you want to charge
                                }
                            }]
                    });
                },
                onApprove: function (data, actions) {
                    // Capture the funds when the customer approves the payment
                    return actions.order.capture().then(function (details) {
                        // Handle successful payment
                        document.getElementById("btnSubmit").disabled = false;
                        alert('Transaction completed by ' + details.payer.name.given_name);
                    });
                }
            }).render('#paypal-button-container');
        </script>
        <jsp:include page="../layouts/footer.jsp"/>

        <jsp:include page="../layouts/body_assets.jsp"/>
    </body>
</html>
