<%-- 
    Document   : createTour.jsp
    Created on : Nov 1, 2023, 10:56:15 PM
    Author     : lenam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="text/html;charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Form</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
              crossorigin="anonymous">
        <style>
            body {
                font-family: sans-serif;
            }

            .container {
                width: 500px;
                margin: 0 auto;
            }

            form {
                padding: 20px;
                border: 1px solid #ccc;
            }

            .form-label {
                font-weight: bold;
            }

            .form-control {
                width: 100%;
                padding: 10px;
                border-radius: 5px;
                border: 1px solid #ff0000;
            }

            textarea {
                height: 100px;
            }

            .btn {
                margin-top: 10px;
            }
        </style>
    </head>

    <body>
        <div class="container">
            <form action="CreateTourServlet" method="POST" enctype="multipart/form-data">
                <div class="mb-3">
                    <label for="title" class="form-label">Tiêu đề</label>
                    <input type="text" class="form-control" id="tName" name="tName">
                </div>
                <div class="mb-3">
                    <label for="image" class="form-label">Hình ảnh</label>
                    <input type="file" class="form-control" id="myFile1" name="myFile1" />

                    <input type="text" id="myFile" class="form-control form-control-lg"
                           name="myFile" hidden=""/>
                    <br>
                    <img id="preview" width="300px" height="190px" />
                </div>
                <div class="mb-3">
                    <label for="description" class="form-label">Mô tả</label>
                    <textarea class="form-control" id="description" name="description" rows="5"></textarea>
                </div>
                <div class="mb-3">
                    <label for="category" class="form-label">Tiêu đề</label>
                    <input type="category" class="form-control" id="category" name="category">
                </div>
                <div class="mb-3">
                    <label for="location" class="form-label">Vị trí</label>
                    <input type="location" class="form-control" id="location" name="location">
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="mb-3">
                            <label for="start_date" class="form-label">Ngày bắt đầu</label>
                            <input type="date" class="form-control" id="startDate" name="startDate">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-3">
                            <label for="end_date" class="form-label">Ngày kết thúc</label>
                            <input type="date" class="form-control" id="endDate" name="endDate">
                        </div>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="price" class="form-label">Giá (1 người)</label>
                    <input type="number" class="form-control" id="tPrice" name="tPrice">
                </div>
                <div class="mb-3">
                    <label for="status" class="form-label">Trạng thái</label>
                    <select class="form-control" id="status" name="status" disabled="">
                        <option value="1">Hoạt động</option>
                        <option value="2">Không hoạt động</option>
                    </select>
                </div>
                <div class="d-flex justify-content-end">
                    <button type="submit" class="btn btn-success mx-2" style="width: 7rem; line-height: 1.5rem; background-color: #31f769;" value="Submit">Create</button>
                    <a href="ViewTourServlet" class="btn btn-danger mx-2" style="width: 7rem; line-height: 1.5rem;">Hủy</a>
                </div>
            </form>
        </div>
    </body>

</html>

<script>
    var input = document.getElementById('myFile1');
    var fileName = document.getElementById('myFile');
    input.addEventListener('change', function () {
        fileName.value = "images\\" + input.files[0].name;
    });
</script>

<script>
    const fileInput = document.getElementById('myFile1');
    const preview = document.getElementById('preview');
    fileInput.addEventListener('change', function () {
        const file = fileInput.files[0];
        const reader = new FileReader();
        reader.addEventListener('load', function () {
            preview.src = reader.result;
        }, false);
        if (file) {
            reader.readAsDataURL(file);
        }
    });
</script>