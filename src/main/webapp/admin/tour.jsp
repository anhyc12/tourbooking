<%-- 
    Document   : tour
    Created on : Oct 31, 2023, 11:31:45 PM
    Author     : lenam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Tours</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
              crossorigin="anonymous">
        <style>
            body {
                font-family: sans-serif;
                background-color: #fff;
            }

            .container {
                width: 800px;
                margin: 0 auto;
            }

            .button-col {
                display: flex;
                flex-direction: column;
                border: none;
            }

            .button-col a {
                margin-bottom: 10px;
            }

            .navbar-side {
                position: fixed;
                top: 0;
                left: 0;
                bottom: 0;
                width: 200px;
                z-index: 1;
                background-color: #fff;
                box-shadow: 0 0 6px rgba(0, 0, 0, 0.25);
            }

            .navbar-side .nav-link {
                color: #000;
                font-size: 14px;
            }

            .navbar-side .nav-link.active {
                background-color: #31f769;
                color: #fff;
            }

        </style>

    </head>
    <body>


        <div style="margin-top: 5%"></div>
        <div class="container">
            <div class="navbar-side">
                <nav class="nav nav-pills flex-column">
                    <a class="nav-link active" href="#" style="background-color:#696cff">Dashboard</a>
                    <a class="nav-link" href="#">User Management</a>
                    <a class="nav-link" href="admin-view-blog">Blog Management</a>
                    <a class="nav-link" href="ViewTourServlet">Tour Management</a>

                </nav>
            </div>
            
            <div class="d-flex justify-content-between">
                <a href="CreateTourServlet" class="btn btn-primary mb-3">Create Tour</a>

                <form class="row" action="ViewTourServlet" method="get">
                    <div class="col-auto">
                        <input class="form-control" type="text" name="search" placeholder="Searching..." />                        
                    </div>
                    <div class="col-auto">
                        <input class="btn btn-primary" type="submit" value="Search" />                        
                    </div>
                </form>
            </div>
            <div style="border: 1px solid #ccc;">
                <c:forEach items="${requestScope.tourList}" var="LPP">
                    <div class="row mt-5 mb-3" style="margin-left: 5%">
                        <div class="col-md-9" style="border: 1px solid #ccc;">
                            <div class="row">
                                <div class="col-md-5 mt-4">
                                    <img src="${LPP.getImage()}"
                                         class="card-img-top mb-3" alt="Hagia Sophia in Istanbul">
                                </div>
                                <div class="card-body col-md-7">
                                    <h2 class="card-title">${LPP.getName()}</h2>
                                    <p class="card-text">${LPP.getDescription()}</p>
                                    <span>
                                        Status: <span style="color: green;">${LPP.getStatusType()}</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 mt-5">
                            <div class="card button-col" style="width: 50%; background-color: white">
                                <a href="#" class="btn btn-primary" style="background-color: #31f769;">Update</a>
                                <span>
                                    <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#del${LPP.getId()}" style="width: 100%">
                                        Delete
                                    </button>
                                    <!-- Modal -->
                                    <div class="modal fade" id="del${LPP.getId()}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">

                                                <div class="p-4 text-center fs-3" 
                                                     style="color: red; border: 1px solid #ccc;">
                                                    You are doing delete this tour 
                                                </div>
                                                <form action="DeleteTourServlet" method="GET">
                                                    <input type="hidden" name="tourID" value="${LPP.getId()}"/>
                                                    <div class="modal-footer justify-content-center">
                                                        <button id="update-profile-btn" type="submit" class="btn btn-danger" style="width: 7rem; line-height: 1.5rem; background-color: #31f769;">Yes</button>
                                                        <button type="button" class="btn btn-danger" data-bs-dismiss="modal" style="width: 7rem; line-height: 1.5rem;">No</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </span>                        
                            </div>
                        </div>
                    </div>

                </c:forEach>
            </div>
        </div>



        <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        crossorigin="anonymous"></script>
    </body>
</html>

