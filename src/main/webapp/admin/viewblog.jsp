<%-- Document : admin Created on : Feb 9, 2023, 12:48:02 AM Author : ASUS --%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en" class="light-style layout-menu-fixed" dir="ltr" data-theme="theme-default" data-assets-path="assets/"
      data-template="vertical-menu-template-free">

    <head>
        <meta charset="utf-8" />
        <meta name="viewport"
              content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />

        <title>Dashboard - Analytics | Sneat - Bootstrap 5 HTML Admin Template - Pro</title>

        <meta name="description" content="" />

        <!-- include libraries(jQuery, bootstrap) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <style>
            a:hover {
                text-decoration: none;
            }
            .navbar-side {
                position: fixed;
                top: 0;
                left: 0;
                bottom: 0;
                width: 200px;
                z-index: 1;
                background-color: #fff;
                box-shadow: 0 0 6px rgba(0, 0, 0, 0.25);
            }

            .navbar-side .nav-link {
                color: #000;
                font-size: 14px;
            }

            .navbar-side .nav-link.active {
                background-color: #31f769;
                color: #fff;
            }
        </style>

        <!-- Favicon -->
        <!--<link rel="icon" type="image/x-icon" href="assets/img/favicon/favicon.ico" />-->

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
        <link
            href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap"
            rel="stylesheet" />

        <!-- Icons. Uncomment required icon fonts -->
        <link rel="stylesheet" href="assets/vendor/fonts/boxicons.css" />

        <!-- Core CSS -->
        <link rel="stylesheet" href="assets/vendor/css/core.css" class="template-customizer-core-css" />
        <link rel="stylesheet" href="assets/vendor/css/theme-default.css" class="template-customizer-theme-css" />
        <link rel="stylesheet" href="assets/css/demo.css" />

        <!-- Vendors CSS -->
        <link rel="stylesheet" href="assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css" />

        <link rel="stylesheet" href="assets/vendor/libs/apex-charts/apex-charts.css" />

        <!-- Page CSS -->

        <!-- Helpers -->
        <script src="assets/vendor/js/helpers.js"></script>

        <!--! Template customizer & Theme config files MUST be included after core stylesheets and helpers.js in the <head> section -->
        <!--? Config:  Mandatory theme config file contain global vars & default theme options, Set your preferred theme option in this file.  -->
        <script src="assets/js/config.js"></script>
    </head>

    <body>
        <!-- Layout wrapper -->
        <div class="layout-wrapper layout-content-navbar">
            <div class="layout-container">
                <!-- Menu -->
                <%--<%@ include file="/admin/Template/menu.jsp" %>--%>
                <div class="navbar-side">
                    <nav class="nav nav-pills flex-column">
                        <a class="nav-link active" href="#" style="background-color:#696cff">Dashboard</a>
                        <a class="nav-link" href="#">User Management</a>
                        <a class="nav-link" href="admin-view-blog">Blog Management</a>
                        <a class="nav-link" href="ViewTourServlet">Tour Management</a>
                    </nav>
                </div>
                <!-- / Menu -->

                <!-- Layout container -->
                <div class="layout-page">
                    <!-- Navbar -->

                    <%--<%@ include file="/admin/Template/navbar.jsp" %>--%>


                    <!-- / Navbar -->

                    <!-- Content wrapper -->
                    <div class="content-wrapper">
                        <!-- Content -->

                        <!-- Hoverable Table rows -->
                        <div class="card" style="margin: 2.5%">
                            <h5 class="card-header">Blog Manager</h5>
                            <div class="mb-3 d-flex justify-content-between">
                                <form class="ms-3 row" action="admin-view-blog" method="get">
                                    <div class="col-auto">
                                        <input class="form-control" type="text" name="search" placeholder="Searching..." />                        
                                    </div>
                                    <div class="col-auto">
                                        <input class="btn btn-primary" type="submit" value="Search" />                        
                                    </div>
                                </form>

                                <a class="me-3 btn rounded-pill btn-danger" href="admin-create-blog">Add Blog</a>                                
                            </div>

                            <div class="table-responsive text-nowrap">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Image</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody class="table-border-bottom-0">
                                        <c:forEach items="${news}" var="news">
                                            <tr>
                                                <td>${news.title}</td>
                                                <td>
                                                    <div class="avatar avatar-lg me-2">
                                                        <img src="images/blogs/${news.getBlogId()}/${news.getImage()}" 
                                                             alt="avatar" class="img-thumbnail" style="object-fit: contain"/>
                                                    </div>
                                                </td>

                                                <td><div><span class="badge bg-label-${news.getStatusId().equals("ok")?"success":"danger"} me-1">${news.getStatusId().equals("ok")?"ACTIVE":"DEACTIVE"}</span></div></td>
                                                <td>
                                                    <a title="Edit" class="btn btn-sm btn-outline-primary" href="admin-update-news?newsID=${news.getBlogId()}">
                                                        <i class="bx bx-edit-alt me-1"></i>
                                                    </a>
                                                    <a title="${news.getStatusId().equals("ok")?"Deactivate":"Active"}" class="btn btn-sm btn-outline-${news.getStatusId().equals("ok")?"danger":"success"}" href="admin-change-blog-status?blogid=${news.getBlogId()}">
                                                        <i class="bx bx-${news.getStatusId()?"hide":"show"} me-1"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        </c:forEach>                                    
                                    </tbody>
                                </table>
                                <nav aria-label="Page navigation">
                                    <ul class="pagination justify-content-end me-3">
                                        <c:if test="${size != 0}">
                                            <li class="page-item prev">
                                                <a class="page-link" href="admin-view-blogs?page-type=${pageType}&page=${page-1==0?size:page-1}&name=${name}"><i class="tf-icon bx bx-chevrons-left"></i></a>
                                            </li>
                                        </c:if>
                                        <c:forEach begin="1" end="${size}" var="i">
                                            <li class="page-item ${i==page?"active":""}">
                                                <a class="page-link" href="admin-view-blogs?page-type=${pageType}&page=${i}&name=${name}">${i}</a>
                                            </li>
                                        </c:forEach>
                                        <c:if test="${size != 0}">
                                            <li class="page-item next">
                                                <a class="page-link" href="admin-view-blogs?page-type=${pageType}&page=${page+1>size?1:page+1}&name=${name}"><i class="tf-icon bx bx-chevrons-right"></i></a>
                                            </li>
                                        </c:if>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <!--/ Hoverable Table rows -->
                        <!-- / Content -->

                        <!-- Footer -->
                        <%--<%@ include file="/AdminPage/Template/footer.jsp" %>--%>
                        <!-- / Footer -->

                        <div class="content-backdrop fade"></div>
                    </div>
                    <!-- Content wrapper -->
                </div>
                <!-- / Layout page -->
            </div>
            <style>
                .avatar-lg:hover img {
                    transform: scale(1.5);
                    transition: 0.5s;
                }
            </style>

            <!-- Overlay -->
            <div class="layout-overlay layout-menu-toggle"></div>
        </div>
        <!-- / Layout wrapper -->

        <!-- Core JS -->
        <!-- build:js assets/vendor/js/core.js -->
        <script src="assets/vendor/libs/jquery/jquery.js"></script>
        <script src="assets/vendor/libs/popper/popper.js"></script>
        <script src="assets/vendor/js/bootstrap.js"></script>
        <script src="assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js"></script>

        <script src="assets/vendor/js/menu.js"></script>
        <!-- endbuild -->

        <!-- Vendors JS -->
        <script src="assets/vendor/libs/apex-charts/apexcharts.js"></script>

        <!-- Main JS -->
        <script src="assets/js/main.js"></script>

        <!-- Page JS -->
        <script src="assets/js/dashboards-analytics.js"></script>

        <!-- Place this tag in your head or just before your close body tag. -->
        <script async defer src="https://buttons.github.io/buttons.js"></script>
    </body>

</html>